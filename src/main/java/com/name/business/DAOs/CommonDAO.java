package com.name.business.DAOs;

import com.name.business.entities.Common;
import com.name.business.mappers.CommonMapper;
import com.name.business.representations.CommonDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(CommonMapper.class)
public interface CommonDAO {

    @SqlQuery("SELECT ID_COMMON FROM COMMON WHERE ID_COMMON IN (SELECT MAX(COMMON.ID_COMMON) FROM COMMON)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO COMMON ( COMMON_STATE,CREATION_DATE,MODIFY_DATE) VALUES (:co_st.state,:co_st.creation_date,:co_st.modify_date)")
    void create(@BindBean("co_st")CommonDTO commonDTO);

    /**
     *
     * @param common
     * @return
     */
    @SqlQuery("SELECT * FROM COMMON " +
            " WHERE ( ID_COMMON = :co_st.id_common OR :co_st.id_common IS NULL) AND " +
            "       ( COMMON_STATE =:co_st.state OR :co_st.state IS NULL ) AND " +
            "       ( CREATION_DATE = :co_st.creation_date OR :co_st.creation_date IS NULL ) AND " +
            "       ( MODIFY_DATE = :co_st.modify_date OR :co_st.modify_date IS NULL )")
    List<Common> read(@BindBean("co_st") Common common);

    /**
     *
     * @param id_common
     * @param commonDTO
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    COMMON_STATE =:co_st.state," +
            "    CREATION_DATE = :co_st.creation_date ," +
            "    MODIFY_DATE = :co_st.modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int update(@Bind("id_common") Long id_common, @BindBean("co_st") CommonDTO commonDTO);

    /**
     * Delete: Change State
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    COMMON_STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int delete(@Bind("id_common") Long id_common, @Bind("modify_date") Date date);

    /**
     * Delete: Permanent!
     * @param common
     * @return
     */
    @SqlUpdate("DELETE FROM COMMON" +
            "WHERE ( COMMON_STATE = :co_st.state OR :co_st.state IS NULL ) AND " +
            "      ( CREATION_DATE = :co_st.creation_date OR :co_st.creation_date IS NULL )AND " +
            "      ( MODIFY_DATE = :co_st.modify_date OR :co_st.modify_date IS NULL ) AND " +
            "      ( ID_COMMON = :co_st.id_common OR :co_st.id_common IS NULL ) ")
    int permanentDelete( @BindBean("co_st") Common common);
}
