package com.name.business.DAOs;

import com.name.business.entities.complete.DetailOrderComplete;
import com.name.business.entities.DetailOrder;
import com.name.business.mappers.complete.DetailOrderCompleteMapper;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.representations.DetailOrderDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(DetailOrderCompleteMapper.class)
public interface DetailOrderDAO {

    @SqlQuery("SELECT ID_DETAIL_ORDER FROM DETAIL_ORDER " +
            "WHERE ID_DETAIL_ORDER IN (SELECT MAX(DETAIL_ORDER.ID_DETAIL_ORDER) " +
            "FROM DETAIL_ORDER)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO DETAIL_ORDER ( ID_ORDER, ID_PRODUCT_THIRD ,QUANTITY,ID_THIRD, VALUE_PRODUCT, ID_COMMON) " +
            "VALUES (:detailOrderDTO.id_order , :detailOrderDTO.id_product_third ,:detailOrderDTO.quantity ,:detailOrderDTO.id_third, :detailOrderDTO.value_product ,:id_common)")
    void create(@BindBean("detailOrderDTO") DetailOrderDTO detailOrderDTO, @Bind("id_common") Long id_common);

    @SqlQuery("SELECT * FROM V_DETAIL_ORDER " +
            " WHERE ( " +
            "(V_DETAIL_ORDER.ID_DETAIL_ORDER = :detailOrderComplete.id_detail_order OR :detailOrderComplete.id_detail_order IS NULL) AND" +
            "(V_DETAIL_ORDER.ID_ORDER = :detailOrderComplete.id_order OR :detailOrderComplete.id_order IS NULL) AND" +
            "(V_DETAIL_ORDER.ID_PRODUCT_THIRD = :detailOrderComplete.id_product_third OR :detailOrderComplete.id_product_third IS NULL) AND" +
            "(V_DETAIL_ORDER.QUANTITY = :detailOrderComplete.quantity OR :detailOrderComplete.quantity IS NULL) AND" +
            "(V_DETAIL_ORDER.ID_THIRD = :detailOrderComplete.id_third OR :detailOrderComplete.id_third IS NULL) AND" +
            "(V_DETAIL_ORDER.VALUE_PRODUCT = :detailOrderComplete.value_product OR :detailOrderComplete.value_product IS NULL) AND" +

            "(V_DETAIL_ORDER.COMMON_DETORD = :detailOrderComplete.id_common OR :detailOrderComplete.id_common IS NULL) AND" +
            "(V_DETAIL_ORDER.STATE_DETORD = :detailOrderComplete.common_state OR :detailOrderComplete.common_state IS NULL) AND" +
            "(V_DETAIL_ORDER.CREATE_DETORD = :detailOrderComplete.creation_date OR :detailOrderComplete.creation_date IS NULL) AND" +
            "(V_DETAIL_ORDER.MODIFY_DETORD = :detailOrderComplete.modify_date OR :detailOrderComplete.modify_date IS NULL)" +
            ")")
    List<DetailOrderComplete> read(@BindBean("detailOrderComplete") DetailOrderComplete detailOrderComplete);

    @SqlQuery("SELECT ID_COMMON FROM DETAIL_ORDER " +
            "  WHERE (ID_DETAIL_ORDER = :idDetailOrder OR :idDetailOrder IS NULL)")
    List<Long> readCommons(@Bind("idDetailOrder") Long idDetailOrder);

    @SqlUpdate("UPDATE DETAIL_ORDER SET " +
            "ID_ORDER = :detailOrderDTO.id_order OR :detailOrderDTO.id_order, " +
            "ID_PRODUCT_THIRD = :detailOrderDTO.id_product_third OR :detailOrderDTO.id_product_third, " +
            "QUANTITY = :detailOrderDTO.quantity OR :detailOrderDTO.quantity, " +
            "ID_THIRD = :detailOrderDTO.id_third OR :detailOrderDTO.id_third, " +
            "VALUE_PRODUCT = :detailOrderDTO.value_product OR :detailOrderDTO.value_product " +
            "WHERE (" +
            "ID_DETAIL_ORDER = :id)")
    int update(@Bind("id") Long id, @BindBean("detailOrderDTO") DetailOrderDTO detailOrderDTO);

    /**
     * Delete: Change State
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int delete(@Bind("id_common") Long id_common, @Bind("modify_date") Date date);

    @SqlUpdate("DELETE FROM FROM DETAIL_ORDER" +
            " WHERE ( " +
            "(DETAIL_ORDER.ID_DETAIL_ORDER = :detailOrder.id_detail_order OR :detailOrder.id_detail_order IS NULL) AND" +
            "(DETAIL_ORDER.ID_ORDER = :detailOrder.id_order OR :detailOrder.id_order IS NULL) AND" +
            "(DETAIL_ORDER.ID_PRODUCT_THIRD = :detailOrder.id_product_third OR :detailOrder.id_product_third NULL) AND" +
            "(DETAIL_ORDER.QUANTITY = :detailOrder.quantity OR :detailOrder.quantity IS NULL) AND" +
            "(DETAIL_ORDER.ID_THIRD = :detailOrder.id_third OR :detailOrder.id_third IS NULL) AND" +
            "(DETAIL_ORDER.VALUE_PRODUCT = :detailOrder.value_product OR :detailOrder.value_product IS NULL) AND" +
            "(DETAIL_ORDER.ID_COMMON = :detailOrder.id_common OR :detailOrder.id_common IS NULL)" +
            ")")
    int permanentDelete(@BindBean("detailOrder") DetailOrder detailOrder);
}
