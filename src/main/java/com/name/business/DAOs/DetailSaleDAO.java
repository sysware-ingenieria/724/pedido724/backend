package com.name.business.DAOs;

import com.name.business.entities.complete.DetailSaleComplete;
import com.name.business.entities.DetailSale;
import com.name.business.mappers.complete.DetailSaleCompleteMapper;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.representations.DetailSaleDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(DetailSaleCompleteMapper.class)
public interface DetailSaleDAO {

    @SqlQuery("SELECT ID_DETAIL_SALE FROM DETAIL_SALE " +
            "WHERE ID_DETAIL_SALE IN (SELECT MAX(SALE.ID_DETAIL_SALE) " +
            "FROM DETAIL_SALE)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO DETAIL_SALE ( ID_SALE,ID_COMMON,ID_THIRD, ID_PRODUCT_THIRD, ORIGINAL_VALUE, SALE_VALUE, QUANTITY) " +
            "VALUES (:detailSaleDTO.id_sale, :id_common, :detailSaleDTO.id_third, :detailSaleDTO.id_product_third, :detailSaleDTO.original_value, :detailSaleDTO.sale_value, :detailSaleDTO.quantity )")
    void create(@BindBean("detailSaleDTO") DetailSaleDTO detailSaleDTO, @Bind("id_common") Long id_common);

    @SqlQuery("SELECT * FROM V_DETAIL_SALE " +
            " WHERE ( " +
            "(V_DETAIL_SALE.ID_DETAIL_SALE = :detailSaleComplete.id_detail_sale OR :detailSaleComplete.id_detail_sale IS NULL) AND" +
            "(V_DETAIL_SALE.ID_SALE = :detailSaleComplete.id_sale OR :detailSaleComplete.id_sale IS NULL) AND" +
            "(V_DETAIL_SALE.ID_THIRD = :detailSaleComplete.id_third OR :detailSaleComplete.id_third IS NULL) AND" +
            "(V_DETAIL_SALE.ID_PRODUCT_THIRD = :detailSaleComplete.id_product_third OR :detailSaleComplete.id_product_third IS NULL) AND" +
            "(V_DETAIL_SALE.ORIGINAL_VALUE = :detailSaleComplete.original_value OR :detailSaleComplete.original_value IS NULL) AND" +
            "(V_DETAIL_SALE.SALE_VALUE = :detailSaleComplete.sale_value OR :detailSaleComplete.sale_value IS NULL) AND" +
            "(V_DETAIL_SALE.QUANTITY = :detailSaleComplete.quantity OR :detailSaleComplete.quantity IS NULL) AND" +

            "(V_DETAIL_SALE.COMMON_DETSAL = :detailSaleComplete.id_common OR :detailSaleComplete.id_common IS NULL) AND" +
            "(V_DETAIL_SALE.STATE_DETSAL = :detailSaleComplete.common_state OR :detailSaleComplete.common_state IS NULL) AND" +
            "(V_DETAIL_SALE.CREATE_DETSAL = :detailSaleComplete.creation_date OR :detailSaleComplete.creation_date IS NULL) AND" +
            "(V_DETAIL_SALE.MODIFY_DETSAL = :detailSaleComplete.modify_date OR :detailSaleComplete.modify_date IS NULL)" +
            ")")
    List<DetailSaleComplete> read(@BindBean("detailSaleComplete") DetailSaleComplete detailSaleComplete);

    @SqlQuery("SELECT ID_COMMON FROM DETAIL_SALE " +
            "  WHERE (ID_DETAIL_SALE = :idDetailSale OR :idDetailSale IS NULL)")
    List<Long> readCommons(@Bind("idDetailSale") Long idDetailSale);

    @SqlUpdate("UPDATE DETAIL_SALE SET " +
            "ID_SALE = :saleDTO.id_sale OR :saleDTO.id_sale, " +
            "ID_THIRD = :saleDTO.id_third OR :saleDTO.id_third, " +
            "ID_PRODUCT_THIRD = :saleDTO.id_product_third OR :saleDTO.id_product_third, " +
            "ORIGINAL_VALUE = :saleDTO.original_value OR :saleDTO.original_value, " +
            "SALE_VALUE = :saleDTO.sale_value OR :saleDTO.sale_value, " +
            "QUANTITY = :saleDTO.quantity OR :saleDTO.quantity " +
            "WHERE (" +
            "ID_DETAIL_SALE = :id)")
    int update(@Bind("id") Long id, @BindBean("detailSaleDTO") DetailSaleDTO detailSaleDTO);

    /**
     * Delete: Change State
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int delete(@Bind("id_common") Long id_common, @Bind("modify_date") Date date);

    @SqlUpdate("DELETE FROM DETAIL_SALE" +
            " WHERE ( " +
            "(DETAIL_SALE.ID_DETAIL_SALE = :detailSale.id_detail_sale OR :detailSale.id_detail_sale IS NULL) AND" +
            "(DETAIL_SALE.ID_SALE = :detailSale.id_sale OR :detailSale.id_sale IS NULL) AND" +
            "(DETAIL_SALE.ID_COMMON = :detailSale.id_common OR :detailSale.id_common IS NULL) AND" +
            "(DETAIL_SALE.ID_THIRD = :detailSale.id_third OR :detailSale.id_third IS NULL) AND" +
            "(DETAIL_SALE.ID_PRODUCT_THIRD = :detailSale.id_product_third OR :detailSale.id_product_third IS NULL) AND" +
            "(DETAIL_SALE.ORIGINAL_VALUE = :detailSale.original_value OR :detailSale.original_value IS NULL) AND" +
            "(DETAIL_SALE.SALE_VALUE = :detailSale.sale_value OR :detailSale.sale_value IS NULL) AND" +
            "(DETAIL_SALE.QUANTITY = :detailSale.quantity OR :detailSale.quantity IS NULL)" +
            ")")
    int permanentDelete(@BindBean("detailSale") DetailSale detailSale);
}
