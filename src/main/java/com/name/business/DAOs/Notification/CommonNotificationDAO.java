package com.name.business.DAOs.Notification;

import com.name.business.entities.notification.CommonNotification;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.representations.notification.CommonNotificationDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(CommonNotificationMapper.class)
public interface CommonNotificationDAO {

    @SqlQuery("SELECT ID_COMMON_NOTIF FROM COMMON_NOTIFICATION " +
            "WHERE ID_COMMON_NOTIF IN (SELECT MAX(COMMON_NOTIFICATION.ID_COMMON_NOTIF) " +
            "FROM COMMON_NOTIFICATION)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO COMMON_NOTIFICATION ( STATE_NOTIF,CREATION_DATE,MODIFY_DATE) " +
            "VALUES (:comnot.state_notif,:comnot.creation_date,:comnot.modify_date)")
    void create(@BindBean("comnot") CommonNotificationDTO commonNotificationDTO);

    /**
     *
     * @param commonNotification
     * @return
     */
    @SqlQuery("SELECT * FROM COMMON_NOTIFICATION " +
            " WHERE ( ID_COMMON_NOTIF = :comnot.id_common_notif OR :comnot.id_common_notif IS NULL) AND " +
            "       ( STATE_NOTIF =:comnot.state_notif OR :comnot.state_notif IS NULL ) AND " +
            "       ( CREATION_DATE = :comnot.creation_date OR :comnot.creation_date IS NULL ) AND " +
            "       ( MODIFY_DATE = :comnot.modify_date OR :comnot.modify_date IS NULL )")
    List<CommonNotification> read(@BindBean("comnot") CommonNotification commonNotification);

    /**
     *
     * @param id_common
     * @param commonNotificationDTO
     * @return
     */
    @SqlUpdate("UPDATE COMMON_NOTIFICATION SET " +
            "    STATE_NOTIF =:comnot.state_notif," +
            "    CREATION_DATE = :comnot.creation_date ," +
            "    MODIFY_DATE = :comnot.modify_date" +
            "  WHERE  (ID_COMMON_NOTIF = :id_common_notif)")
    int update(@Bind("id_common") Long id_common, @BindBean("comnot") CommonNotificationDTO commonNotificationDTO);

    /**
     * Delete: Change State
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON_NOTIFICATION SET " +
            "    STATE_NOTIF =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON_NOTIF = :id_common_notif)")
    int delete(@Bind("id_common_notif") Long id_common, @Bind("modify_date") Date date);

    /**
     * Delete: Permanent!
     * @param common
     * @return
     */
    @SqlUpdate("DELETE FROM COMMON_NOTIFICATION" +
            "WHERE ( STATE_NOTIF = :comnot.state_notif OR :comnot.state_notif IS NULL ) AND " +
            "      ( CREATION_DATE = :comnot.creation_date OR :comnot.creation_date IS NULL )AND " +
            "      ( MODIFY_DATE = :comnot.modify_date OR :comnot.modify_date IS NULL ) AND " +
            "      ( ID_COMMON_NOTIF = :comnot.id_common_notif OR :comnot.id_common_notif IS NULL )")
    int permanentDelete(@BindBean("comnot") CommonNotification common);
}
