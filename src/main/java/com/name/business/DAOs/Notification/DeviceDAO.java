package com.name.business.DAOs.Notification;

import com.name.business.entities.notification.Device;
import com.name.business.entities.notification.FCM;
import com.name.business.entities.notification.complete.DeviceComplete;
import com.name.business.entities.notification.complete.FCMComplete;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.mappers.notification.DeviceMapper;
import com.name.business.representations.notification.DeviceDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(DeviceMapper.class)
public interface DeviceDAO {

    @SqlQuery("SELECT ID_DEVICE FROM DEVICE " +
            "WHERE ID_DEVICE IN (SELECT MAX(DEVICE.ID_DEVICE) " +
            "FROM DEVICE)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO DEVICE ( DEVICE_NUMBER,UUID,DEVICE_TYPE, ID_COMMON) " +
            "VALUES (:deviceDTO.device_number, :deviceDTO.uuid, :deviceDTO.device_type, :id_common)")
    void create(@BindBean("deviceDTO") DeviceDTO deviceDTO, @Bind("id_common") Long id_common);

    /**
     *
     * @param
     * @return
     */
    @SqlQuery("SELECT * FROM V_DEVICECOMMON " +
            " WHERE ( " +
            "(V_DEVICECOMMON.ID_DEVICE = :deviceComplete.id_device OR :deviceComplete.id_device IS NULL) AND" +
            "(V_DEVICECOMMON.DEVICE_NUMBER = :deviceComplete.device_number OR :deviceComplete.device_number IS NULL) AND" +
            "(V_DEVICECOMMON.UUID = :deviceComplete.uuid OR :deviceComplete.uuid IS NULL) AND" +
            "(V_DEVICECOMMON.DEVICE_TYPE = :deviceComplete.device_type OR :deviceComplete.device_type IS NULL) AND" +
            "(V_DEVICECOMMON.ID_COMMON = :deviceComplete.id_common_notification OR :deviceComplete.id_common_notification IS NULL) AND" +
            "(V_DEVICECOMMON.STATE_NOTIF = :deviceComplete.common_state OR :deviceComplete.common_state IS NULL) AND" +
            "(V_DEVICECOMMON.CREATION_DATE = :deviceComplete.creation_date OR :deviceComplete.creation_date IS NULL) AND" +
            "(V_DEVICECOMMON.MODIFY_DATE = :deviceComplete.modify_date OR :deviceComplete.modify_date IS NULL)" +
            ")")
    List<DeviceComplete> read(@BindBean("deviceComplete") DeviceComplete deviceComplete);

    @SqlQuery("SELECT ID_COMMON FROM DEVICE " +
            "  WHERE (ID_DEVICE = :idDevice OR :idDevice IS NULL)")
    List<Long> readCommons(@Bind("idDevice") Long idDevice);

    @SqlUpdate("UPDATE DEVICE SET " +
            "DEVICE_NUMBER = :deviceDTO.device_number OR :deviceDTO.device_number, " +
            "UUID = :deviceDTO.uuid OR :deviceDTO.uuid, " +
            "DEVICE_TYPE = :deviceDTO.device_type OR :deviceDTO.device_type " +
            "WHERE (" +
            "ID_DEVICE = :id)" )
    int update(@Bind("id") Long id, @BindBean("deviceDTO") DeviceDTO deviceDTO);
 
    @SqlUpdate("UPDATE COMMON_NOTIFICATION SET " +
            "    STATE_NOTIF =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON_NOTIF = :id_common_notif)")
    int delete(@Bind("id_common_notif") Long id_common, @Bind("modify_date") Date date);
    
    @SqlUpdate("DELETE FROM FROM DEVICE" +
            " WHERE ( " +
            "(DEVICE.ID_DEVICE = :device.id_device OR :device.id_device IS NULL) AND" +
            "(DEVICE.DEVICE_NUMBER = :device.device_number OR :device.device_number IS NULL) AND" +
            "(DEVICE.UUID = :device.uuid OR :device.uuid IS NULL) AND" +
            "(DEVICE.DEVICE_TYPE = :device.device_type OR :device.device_type IS NULL) AND" +
            "(DEVICE.ID_COMMON = :device.id_common OR :device.id_common IS NULL)" +
            ")")
    int permanentDelete(@BindBean("device") Device device);
}
