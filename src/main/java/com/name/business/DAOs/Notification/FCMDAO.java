package com.name.business.DAOs.Notification;

import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.FCM;
import com.name.business.entities.notification.complete.FCMComplete;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.mappers.notification.FCMMapper;
import com.name.business.representations.notification.CommonNotificationDTO;
import com.name.business.representations.notification.FCMDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(FCMMapper.class)
public interface FCMDAO {

    @SqlQuery("SELECT ID_FCM FROM FCM " +
            "WHERE ID_FCM IN (SELECT MAX(FCM.ID_FCM) " +
            "FROM FCM)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO FCM ( TITLE,DESCRIPTION,DATAINFO, STATE_FCM, TYPE_FCM, SEND_DATE, ID_COMMON) " +
            "VALUES (:fcmdto.title, :fcmdto.description, :fcmdto.data_info, :fcmdto.state_fcm, :fcmdto.type , :fcmdto.date_sent, :id_common)")
    void create(@BindBean("fcmdto") FCMDTO fcmdto, @Bind("id_common") Long id_common);

    /**
     *
     * @param
     * @return
     */
    @SqlQuery("SELECT * FROM V_FCMCOMMON " +
            " WHERE ( " +
            "(V_FCMCOMMON.ID_FCM = :fcmComplete.id OR :fcmComplete.id IS NULL) AND" +
            "(V_FCMCOMMON.TITLE = :fcmComplete.title OR :fcmComplete.title IS NULL) AND" +
            "(V_FCMCOMMON.DESCRIPTION = :fcmComplete.description OR :fcmComplete.description IS NULL) AND" +
            "(V_FCMCOMMON.DATAINFO = :fcmComplete.data_info OR :fcmComplete.data_info IS NULL) AND" +
            "(V_FCMCOMMON.STATE_FCM = :fcmComplete.state_fcm OR :fcmComplete.state_fcm IS NULL) AND" +
            "(V_FCMCOMMON.SEND_DATE = :fcmComplete.sent_date OR :fcmComplete.sent_date IS NULL) AND" +
            "(V_FCMCOMMON.ID_COMMON = :fcmComplete.id_common_notification OR :fcmComplete.id_common_notification IS NULL) AND" +
            "(V_FCMCOMMON.STATE_NOTIF = :fcmComplete.common_state OR :fcmComplete.common_state IS NULL) AND" +
            "(V_FCMCOMMON.CREATION_DATE = :fcmComplete.creation_date OR :fcmComplete.creation_date IS NULL) AND" +
            "(V_FCMCOMMON.MODIFY_DATE = :fcmComplete.modify_date OR :fcmComplete.modify_date IS NULL)" +
            ")")
    List<FCMComplete> read(@BindBean("fcmComplete") FCMComplete fcmComplete);

    @SqlQuery("SELECT ID_COMMON FROM FCM " +
            "  WHERE (ID_FCM = :idFCM OR :idFCM IS NULL)")
    List<Long> readCommons(@Bind("idFCM") Long idFCM);

    @SqlUpdate("UPDATE FCM SET " +
            "TITLE = :fcmdto.title OR :fcmdto.title, " +
            "DESCRIPTION = :fcmdto.description OR :fcmdto.description, " +
            "DATAINFO = :fcmdto.datainfo OR :fcmdto.datainfo, " +
            "STATE_FCM = :fcmdto.state_fcm OR :fcmdto.state_fcm, " +
            "TYPE_FCM = :fcmdto.type OR :fcmdto.type, " +
            "SEND_DATE = :fcmdto.date_sent OR :fcmdto.date_sent " +
            "WHERE (" +
            "ID_FCM = :id)" )
    int update(@Bind("id") Long id, @BindBean("fcmdto") FCMDTO fcmdto);

 
    @SqlUpdate("UPDATE COMMON_NOTIFICATION SET " +
            "    STATE_NOTIF =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON_NOTIF = :id_common_notif)")
    int delete(@Bind("id_common_notif") Long id_common, @Bind("modify_date") Date date);

    @SqlUpdate("DELETE FROM FROM FCM" +
            " WHERE ( " +
            "(FCM.ID_FCM = :fcmNotif.id OR :fcmNotif.id IS NULL) AND" +
            "(FCM.TITLE = :fcmNotif.title OR :fcmNotif.title IS NULL) AND" +
            "(FCM.DESCRIPTION = :fcmNotif.description OR :fcmNotif.description IS NULL) AND" +
            "(FCM.DATAINFO = :fcmNotif.data_info OR :fcmNotif.data_info IS NULL) AND" +
            "(FCM.STATE_FCM = :fcmNotif.state_fcm OR :fcmNotif.state_fcm IS NULL) AND" +
            "(FCM.SEND_DATE = :fcmNotif.sent_date OR :fcmNotif.sent_date IS NULL) AND" +
            "(FCM.TYPE_FCM = :fcmNotif.type_fcm OR :fcmNotif.type_fcm IS NULL) AND" +
            "(FCM.ID_COMMON = :fcmNotif.id_common_notification OR :fcmNotif.id_common_notification IS NULL)" +
            ")")
    int permanentDelete(@BindBean("fcmNotif") FCM fcm);
}
