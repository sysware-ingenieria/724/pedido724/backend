package com.name.business.DAOs;

import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.Orders;
import com.name.business.mappers.complete.OrderCompleteMapper;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.representations.OrdersDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(OrderCompleteMapper.class)
public interface OrdersDAO {

    @SqlQuery("SELECT ID_ORDER FROM ORDERS " +
            "WHERE ID_ORDER IN (SELECT MAX(ORDERS.ID_ORDER) " +
            "FROM ORDERS)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO ORDERS ( ID_THIRD_CLIENT,ID_STATE,ORDER_NUMBER, ID_SALE, ID_COMMON) " +
            "VALUES (:ordersDTO.id_third_client , :idState, :ordersDTO.order_number ,:ordersDTO.id_sale ,:id_common)")
    void create(@BindBean("ordersDTO") OrdersDTO ordersDTO, @Bind("id_common") Long id_common, @Bind("idState") Long idState);

    @SqlQuery("SELECT * FROM V_ORDER " +
            " WHERE ( " +
            "(V_ORDER.ID_ORDER = :ordersComplete.id_sale OR :ordersComplete.id_sale IS NULL) AND" +
            "(V_ORDER.ID_THIRD_CLIENT = :ordersComplete.id_employee OR :ordersComplete.id_employee IS NULL) AND" +
            "(V_ORDER.ORDER_NUMBER = :ordersComplete.id_third OR :ordersComplete.id_third IS NULL) AND" +
            "(V_ORDER.ID_SALE = :ordersComplete.begin_date OR :ordersComplete.begin_date IS NULL) AND" +
            "(V_ORDER.FINISH_DATE = :ordersComplete.finish_date OR :ordersComplete.finish_date IS NULL) AND" +
            
            "(V_ORDER.ID_STATE = :ordersComplete.id_state OR :ordersComplete.id_state IS NULL) AND" +
            "(V_ORDER.NAME_STATE = :ordersComplete.name_state OR :ordersComplete.name_state IS NULL) AND" +
            "(V_ORDER.DESCRIPTION_STATE = :ordersComplete.description_state OR :ordersComplete.description_state IS NULL) AND" +
            "(V_ORDER.TYPE_STATE = :ordersComplete.id_common_state OR :ordersComplete.id_common_state IS NULL) AND" +
            "(V_ORDER.STATE_STATE = :ordersComplete.state_state OR :ordersComplete.state_state IS NULL) AND" +
            "(V_ORDER.CREATE_STATE = :ordersComplete.creation_date_state OR :ordersComplete.creation_date_state IS NULL) AND" +
            "(V_ORDER.MODIFY_STATE = :ordersComplete.modify_date_state OR :ordersComplete.modify_date_state IS NULL) AND" +

            "(V_ORDER.COMMON_ORDER = :ordersComplete.id_common OR :ordersComplete.id_common IS NULL) AND" +
            "(V_ORDER.STATE_ORDER = :ordersComplete.common_state OR :ordersComplete.common_state IS NULL) AND" +
            "(V_ORDER.CREATION_DATE = :ordersComplete.creation_date OR :ordersComplete.creation_date IS NULL) AND" +
            "(V_ORDER.MODIFY_DATE = :ordersComplete.modify_date OR :ordersComplete.modify_date IS NULL)" +
            ")")
    List<OrdersComplete> read(@BindBean("ordersComplete") OrdersComplete ordersComplete);

    @SqlQuery("SELECT ID_COMMON FROM ORDERS " +
            "  WHERE (ID_ORDER = :idOrder OR :idOrder IS NULL)")
    List<Long> readCommons(@Bind("idOrder") Long idOrder);

    @SqlQuery("SELECT ID_STATE FROM ORDERS " +
            "  WHERE (ID_ORDER = :idOrder OR :idOrder IS NULL)")
    List<Long> readState(@Bind("idOrder") Long idOrder);

    @SqlUpdate("UPDATE ORDERS SET " +
            "ID_THIRD_CLIENT = :ordersDTO.id_third_client OR :ordersDTO.id_third_client, " +
            "ID_STATE = :ordersDTO.order_number OR :ordersDTO.begin_date, " +
            "ORDER_NUMBER = :ordersDTO.stateOrder OR :ordersDTO.stateOrder " +
            "WHERE (" +
            "ID_ORDER = :id)")
    int update(@Bind("id") Long id, @BindBean("ordersDTO") OrdersDTO ordersDTO);


    /**
     * Delete: Change State
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int delete(@Bind("id_common") Long id_common, @Bind("modify_date") Date date);

    @SqlUpdate("DELETE FROM FROM ORDERS" +
            " WHERE ( " +
            "(ORDERS.ID_ORDER = :orders.id OR :orders.id IS NULL) AND" +
            "(ORDERS.ID_THIRD_CLIENT = :orders.title OR :orders.title IS NULL) AND" +
            "(ORDERS.ID_STATE = :orders.description OR :orders.description IS NULL) AND" +
            "(ORDERS.ORDER_NUMBER = :orders.data_info OR :orders.data_info IS NULL) AND" +
            "(ORDERS.ID_SALE = :orders.state_ORDERS OR :orders.state_ORDERS IS NULL) AND" +
            "(ORDERS.ID_COMMON = :orders.sent_date OR :orders.sent_date IS NULL)" +
            ")")
    int permanentDelete(@BindBean("orders") Orders orders);
}
