package com.name.business.DAOs;

import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.Sale;
import com.name.business.mappers.complete.SaleCompleteMapper;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.representations.SaleDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(SaleCompleteMapper.class)
public interface SaleDAO {

    @SqlQuery("SELECT ID_SALE FROM SALE " +
            "WHERE ID_SALE IN (SELECT MAX(SALE.ID_SALE) " +
            "FROM SALE)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO SALE ( ID_EMPLOYEE,ID_THIRD,ID_COMMON, ID_STATE, BEGIN_DATE, FINISH_DATE, NAME_SALE, DESCRIPTION, TOTAL_VALUE, DISCOUNT_PERCENT) " +
            "VALUES (:saleDTO.id_employee, :saleDTO.id_third, :id_common, :idState, :saleDTO.begin_date, :saleDTO.finish_date, :saleDTO.name_sale , :saleDTO.description, :saleDTO.total_value,  :saleDTO.discount_percent)")
    void create(@BindBean("saleDTO") SaleDTO saleDTO, @Bind("id_common") Long id_common, @Bind("idState") Long idState);

    @SqlQuery("SELECT * FROM V_SALE " +
            " WHERE ( " +
            "(V_SALE.ID_SALE = :saleComplete.id_sale OR :saleComplete.id_sale IS NULL) AND" +
            "(V_SALE.ID_EMPLOYEE = :saleComplete.id_employee OR :saleComplete.id_employee IS NULL) AND" +
            "(V_SALE.ID_THIRD = :saleComplete.id_third OR :saleComplete.id_third IS NULL) AND" +
            "(V_SALE.BEGIN_DATE = :saleComplete.begin_date OR :saleComplete.begin_date IS NULL) AND" +
            "(V_SALE.FINISH_DATE = :saleComplete.finish_date OR :saleComplete.finish_date IS NULL) AND" +
            "(V_SALE.NAME_SALE = :saleComplete.name_sale OR :saleComplete.name_sale IS NULL) AND" +
            "(V_SALE.DESCRIPTION_SALE = :saleComplete.description_sale OR :saleComplete.description_sale IS NULL) AND" +
            "(V_SALE.TOTAL_VALUE = :saleComplete.total_value OR :saleComplete.total_value IS NULL) AND" +
            "(V_SALE.DISCOUNT_PERCENT = :saleComplete.discount_percent OR :saleComplete.discount_percent IS NULL) AND" +

            "(V_SALE.ID_STATE = :saleComplete.id_state OR :saleComplete.id_state IS NULL) AND" +
            "(V_SALE.NAME_STATE = :saleComplete.name_state OR :saleComplete.name_state IS NULL) AND" +
            "(V_SALE.DESCRIPTION_STATE = :saleComplete.description_state OR :saleComplete.description_state IS NULL) AND" +
            "(V_SALE.TYPE_STATE = :saleComplete.id_common_state OR :saleComplete.id_common_state IS NULL) AND" +
            "(V_SALE.STATE_STATE = :saleComplete.state_state OR :saleComplete.state_state IS NULL) AND" +
            "(V_SALE.CREATE_STATE = :saleComplete.creation_date_state OR :saleComplete.creation_date_state IS NULL) AND" +
            "(V_SALE.MODIFY_STATE = :saleComplete.modify_date_state OR :saleComplete.modify_date_state IS NULL) AND" +

            "(V_SALE.COMMON_SALE = :saleComplete.id_common OR :saleComplete.id_common IS NULL) AND" +
            "(V_SALE.STATE_SALE = :saleComplete.common_state OR :saleComplete.common_state IS NULL) AND" +
            "(V_SALE.CREATE_SALE = :saleComplete.creation_date OR :saleComplete.creation_date IS NULL) AND" +
            "(V_SALE.MODIFY_SALE = :saleComplete.modify_date OR :saleComplete.modify_date IS NULL)" +
            ")")
    List<SaleComplete> read(@BindBean("saleComplete") SaleComplete saleComplete);


    @SqlQuery("SELECT ID_COMMON FROM SALE " +
            "  WHERE (ID_SALE = :idSale OR :idSale IS NULL)")
    List<Long> readCommons(@Bind("idSale") Long idSale);

    @SqlQuery("SELECT ID_STATE FROM SALE " +
            "  WHERE (ID_SALE = :idSale OR :idSale IS NULL)")
    List<Long> readState(@Bind("idSale") Long idSale);

    @SqlUpdate("UPDATE SALE SET " +
            "ID_EMPLOYEE = :saleDTO.id_employee OR :saleDTO.id_employee, " +
            "ID_THIRD = :saleDTO.id_third OR :saleDTO.id_third, " +
            "BEGIN_DATE = :saleDTO.begin_date OR :saleDTO.begin_date, " +
            "FINISH_DATE = :saleDTO.finish_date OR :saleDTO.finish_date, " +
            "NAME_SALE = :saleDTO.name_sale OR :saleDTO.name_sale, " +
            "DESCRIPTION = :saleDTO.description OR :saleDTO.description, " +
            "TOTAL_VALUE = :saleDTO.total_value OR :saleDTO.total_value, " +
            "DISCOUNT_PERCENT = :saleDTO.discount_percent OR :saleDTO.discount_percent " +
            "WHERE (" +
            "ID_SALE = :id)")
    int update(@Bind("id") Long id, @BindBean("saleDTO") SaleDTO saleDTO);

    /**
     * Delete: Change State
     *
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int delete(@Bind("id_common") Long id_common, @Bind("modify_date") Date date);

    @SqlUpdate("DELETE FROM SALE" +
            " WHERE ( " +
            "(SALE.ID_SALE = :sale.id_sale OR :sale.id_sale IS NULL) AND" +
            "(SALE.ID_EMPLOYEE = :sale.id_employee OR :sale.id_employee IS NULL) AND" +
            "(SALE.ID_THIRD = :sale.id_third OR :sale.id_third IS NULL) AND" +
            "(SALE.ID_COMMON = :sale.id_common OR :sale.id_common IS NULL) AND" +
            "(SALE.ID_STATE = :sale.id_state OR :sale.id_state IS NULL) AND" +
            "(SALE.BEGIN_DATE = :sale.begin_date OR :sale.begin_date IS NULL) AND" +
            "(SALE.FINISH_DATE = :sale.finish_date OR :sale.finish_date IS NULL) AND" +
            "(SALE.NAME_SALE = :sale.name_sale OR :sale.name_sale IS NULL) AND" +
            "(SALE.DESCRIPTION = :sale.description OR :sale.description IS NULL) AND" +
            "(SALE.TOTAL_VALUE = :sale.total_value OR :sale.total_value IS NULL) AND" +
            "(SALE.DISCOUNT_PERCENT = :sale.discount_percent OR :sale.discount_percent IS NULL)" +
            ")")
    int permanentDelete(@BindBean("sale") Sale sale);
}
