package com.name.business.DAOs;

import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.entities.StateOrder;
import com.name.business.mappers.complete.StateOrderCompleteMapper;
import com.name.business.mappers.notification.CommonNotificationMapper;
import com.name.business.representations.StateOrderDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(StateOrderCompleteMapper.class)
public interface StateOrderDAO {

    @SqlQuery("SELECT ID_STATE FROM STATE_ORDER " +
            "WHERE ID_STATE IN (SELECT MAX(STATE_ORDER.ID_STATE) " +
            "FROM STATE_ORDER)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO STATE_ORDER ( NAME_STATE,DESCRIPTION,TYPE_STATE, ID_COMMON) " +
            "VALUES (:stateOrderDTOSanitation.name_state, :stateOrderDTOSanitation.description, :stateOrderDTOSanitation.type_state, :id_common)")
    void create(@BindBean("stateOrderDTOSanitation") StateOrderDTO stateOrderDTO, @Bind("id_common") Long id_common);

    /**
     *
     * @param
     * @return
     */
    @SqlQuery("SELECT * FROM V_STATE_ORDER " +
            " WHERE ( " +
            "(V_STATE_ORDER.ID_STATE = :stateOrderComplete.id_state OR :stateOrderComplete.id_state IS NULL) AND" +
            "(V_STATE_ORDER.NAME_STATE = :stateOrderComplete.name_state OR :stateOrderComplete.name_state IS NULL) AND" +
            "(V_STATE_ORDER.DESCRIPTION = :stateOrderComplete.description OR :stateOrderComplete.description IS NULL) AND" +
            "(V_STATE_ORDER.TYPE_STATE = :stateOrderComplete.type_state OR :stateOrderComplete.type_state IS NULL) AND" +
            "(V_STATE_ORDER.ID_COMMON = :stateOrderComplete.id_common OR :stateOrderComplete.id_common IS NULL) AND" +
            "(V_STATE_ORDER.COMMON_STATE = :stateOrderComplete.common_state OR :stateOrderComplete.common_state IS NULL) AND" +
            "(V_STATE_ORDER.CREATION_DATE = :stateOrderComplete.creation_date OR :stateOrderComplete.creation_date IS NULL) AND" +
            "(V_STATE_ORDER.MODIFY_DATE = :stateOrderComplete.modify_date OR :stateOrderComplete.modify_date IS NULL)" +
            ")")
    List<StateOrderComplete> read(@BindBean("stateOrderComplete") StateOrderComplete stateOrderComplete);

    @SqlQuery("SELECT ID_COMMON FROM STATE_ORDER " +
            "  WHERE (ID_STATE = :idStateOrder OR :idStateOrder IS NULL)")
    List<Long> readCommons(@Bind("idStateOrder") Long idStateOrder);


    @SqlUpdate("UPDATE STATE_ORDER SET " +
            "NAME_STATE = :stateOrderDTOSanitation.name_state OR :stateOrderDTOSanitation.name_state, " +
            "DESCRIPTION = :stateOrderDTOSanitation.description OR :stateOrderDTOSanitation.description, " +
            "TYPE_STATE = :stateOrderDTOSanitation.type_state OR :stateOrderDTOSanitation.type_state " +
            "WHERE (" +
            "ID_STATE = :id)" )
    int update(@Bind("id") Long id, @BindBean("stateOrderDTOSanitation") StateOrderDTO stateOrderDTO);

    /**
     * Delete: Change State
     * @param id_common
     * @param date
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "    STATE =: 0," +
            "    MODIFY_DATE = :modify_date" +
            "  WHERE  (ID_COMMON = :id_common)")
    int delete(@Bind("id_common") Long id_common, @Bind("modify_date") Date date);
    
    @SqlUpdate("DELETE FROM FROM STATE_ORDER" +
            " WHERE ( " +
            "(STATE_ORDER.ID_STATE = :stateOrder.id_state OR :stateOrder.id_state IS NULL) AND" +
            "(STATE_ORDER.NAME_STATE = :stateOrder.name_state OR :stateOrder.name_state IS NULL) AND" +
            "(STATE_ORDER.DESCRIPTION = :stateOrder.description OR :stateOrder.description IS NULL) AND" +
            "(STATE_ORDER.TYPE_STATE = :stateOrder.type_state OR :stateOrder.type_state IS NULL) AND" +
            "(STATE_ORDER.ID_COMMON = :stateOrder.id_common OR :stateOrder.id_common IS NULL)" +
            ")")
    int permanentDelete(@BindBean("stateOrder") StateOrder stateOrder);
}
