package com.name.business;

import com.name.business.DAOs.*;
import com.name.business.DAOs.Notification.CommonNotificationDAO;
import com.name.business.DAOs.Notification.DeviceDAO;
import com.name.business.DAOs.Notification.FCMDAO;
import com.name.business.Threads.FCMThread;
import com.name.business.businesses.*;
import com.name.business.businesses.complex.OrdersComplexBusiness;
import com.name.business.businesses.complex.SaleComplexBusiness;
import com.name.business.businesses.notification.CommonNotificationBusiness;
import com.name.business.businesses.notification.DeviceBusiness;
import com.name.business.businesses.notification.FCMBusiness;
import com.name.business.resources.*;
import com.name.business.resources.complex.OrderComplexResource;
import com.name.business.resources.complex.SaleComplexResource;
import com.name.business.resources.notification.DeviceResource;
import com.name.business.resources.notification.FCMResource;
import com.name.business.utils.Scheduler.SchedulerTimer;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Timer;

import static com.name.business.utils.constans.K.URI_BASE;


public class Orders724 extends Application<Orders724AppConfiguration> {


    public static void main(String[] args) throws Exception {
        new Orders724().run(args);

    }


    @Override
    public void initialize(Bootstrap<Orders724AppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }

    @Override
    public void run(Orders724AppConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE + "/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");


        // Se estable el ambiente de coneccion con JDBI con la base de datos de oracle
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "oracle");


        // Pedidos
        final CommonDAO commonDAO = jdbi.onDemand(CommonDAO.class);
        final CommonBusiness commonBusiness = new CommonBusiness(commonDAO);

        final StateOrderDAO stateOrderDAO = jdbi.onDemand(StateOrderDAO.class);
        final StateOrderBusiness stateOrderBusiness = new StateOrderBusiness(stateOrderDAO, commonBusiness);

        final SaleDAO saleDAO = jdbi.onDemand(SaleDAO.class);
        final SaleBusiness saleBusiness = new SaleBusiness(saleDAO, stateOrderBusiness,commonBusiness);

        final OrdersDAO orderDAO = jdbi.onDemand(OrdersDAO.class);
        final OrdersBusiness ordersBusiness = new OrdersBusiness(orderDAO, saleBusiness, stateOrderBusiness, commonBusiness);

        final DetailSaleDAO detailSaleDAO = jdbi.onDemand(DetailSaleDAO.class);
        final DetailSaleBusiness detailSaleBusiness = new DetailSaleBusiness(detailSaleDAO, saleBusiness, commonBusiness);

        final DetailOrderDAO detailOrderDAO = jdbi.onDemand(DetailOrderDAO.class);
        final DetailOrderBusiness detailOrderBusiness = new DetailOrderBusiness(detailOrderDAO, ordersBusiness, commonBusiness);

        // Complex
        final SaleComplexBusiness saleComplexBusiness = new SaleComplexBusiness(detailSaleBusiness, saleBusiness);
        final OrdersComplexBusiness ordersComplexBusiness = new OrdersComplexBusiness(detailOrderBusiness, ordersBusiness);

        // Notification
        final CommonNotificationDAO commonNotificationDAO = jdbi.onDemand(CommonNotificationDAO.class);
        final CommonNotificationBusiness commonNotificationBusiness = new CommonNotificationBusiness(commonNotificationDAO);

        final DeviceDAO deviceDAO = jdbi.onDemand(DeviceDAO.class);
        final DeviceBusiness deviceBusiness = new DeviceBusiness(deviceDAO, commonNotificationBusiness);
        
        final FCMDAO fcmDAO = jdbi.onDemand(FCMDAO.class);
        final FCMBusiness fcmBusiness = new FCMBusiness(fcmDAO, commonNotificationBusiness);

        // Configuration
        Orders724DatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);

        // Pedidos Services
        environment.jersey().register(new StateOrderResource(stateOrderBusiness));
        environment.jersey().register(new SaleResource(saleBusiness));
        environment.jersey().register(new OrderResource(ordersBusiness));
        environment.jersey().register(new DetailSaleResource(detailSaleBusiness));
        environment.jersey().register(new DetailOrderResource(detailOrderBusiness));

        // Complex Services
        environment.jersey().register(new OrderComplexResource(ordersComplexBusiness));
        environment.jersey().register(new SaleComplexResource(saleComplexBusiness));

        //Notification Services
        environment.jersey().register(new DeviceResource(deviceBusiness));
        environment.jersey().register(new FCMResource(fcmBusiness));
        
        // Notification Services
        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);

        final FCMThread fcmThread = new FCMThread(fcmBusiness);
        SchedulerTimer.scheduleTask(fcmThread,"2017-09-27 07:00:00", SchedulerTimer.ONCE_PER_DAY);

    }

    /**
     * @Método: Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     */
    private BasicDataSource createDataSource(Orders724DatabaseConfiguration store724DatabaseConfiguration) {

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(store724DatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(store724DatabaseConfiguration.getUrl());
        basicDataSource.setUsername(store724DatabaseConfiguration.getUsername());
        basicDataSource.setPassword(store724DatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }


}
