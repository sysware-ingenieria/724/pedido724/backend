package com.name.business;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
/**
 * La clase Store724AppConfiguration Se establece los parámetros de  configuración que se requieren
 * con la base de datos(MySql)
 * */
public class Orders724AppConfiguration extends Configuration {


    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    @NotNull
    private Orders724DatabaseConfiguration databaseConfiguration;


    public DataSourceFactory getDataSourceFactory(){
        return database;
    }
    public Orders724DatabaseConfiguration getDatabaseConfiguration(){
        return databaseConfiguration;
    }

}
