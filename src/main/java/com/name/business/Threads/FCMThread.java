package com.name.business.Threads;

import com.name.business.businesses.notification.FCMBusiness;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.complete.FCMComplete;
import com.name.business.representations.notification.CommonNotificationDTO;
import com.name.business.representations.notification.FCMDTO;
import com.name.business.services.google.FCMNotification;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import java.util.List;
import java.util.TimerTask;

/**
 */
public class FCMThread extends TimerTask {
    private FCMBusiness fcmBusiness;

    public FCMThread(FCMBusiness fcmBusiness) {
        super();
        this.fcmBusiness = fcmBusiness;
    }


    @Override
    public boolean cancel() {
        return super.cancel();
    }

    @Override
    public long scheduledExecutionTime() {
        return super.scheduledExecutionTime();
    }

    @Override
    public void run() {
        Either<IException, List<FCMComplete>> fcm = fcmBusiness.get(new FCMComplete(null, null,
                null, null, null, null,
                null, new CommonNotification(null,
                null, null, null)));
        if (fcm.isRight()) {
            beginFCM(fcm.right().value());
        }
    }

    private void beginFCM(List<FCMComplete> value) {
        for (FCMComplete fcm : value) {
            if (!fcm.getState_fcm().equals("ENVIADO")) {
                sentFCMDevise(fcm);
                fcm.setState_fcm("ENVIADO");

                CommonNotification commonFCM = fcm.getCommon();
                FCMDTO fcmdto = new FCMDTO(
                        fcm.getTitle(),
                        fcm.getDescription(),
                        fcm.getData_info(),
                        fcm.getState_fcm(),
                        fcm.getType_fcm(),
                        fcm.getSent_date(),
                        new CommonNotificationDTO(
                                commonFCM.getState(),
                                commonFCM.getCreation_date(),
                                commonFCM.getModify_date()
                        )
                );

                fcmBusiness.update(fcm.getId(), fcmdto);
            }
        }
    }

    private void sentFCMDevise(FCMComplete fcmModel) {

        FCMNotification fcmNotification = new FCMNotification();
        fcmNotification.buildInfo(fcmModel.getTitle(), fcmModel.getDescription(), fcmNotification.DEFAULT, null, "com.ingenieria.sysware.order724_TARGET_NOTIFICATION", null);
        // Notificacion de prueba
        // fcmNotification.buildInfo("Titulo", "Descripcion ", fcmNotification.DEFAULT, "#FFFF0000", "com.ingenieria.sysware.order724_TARGET_NOTIFICATION");
        try {
            fcmNotification.pushFCMNotification("AAAAab3MCDY:APA91bHvj-DX1sCjzNhkcmuk-ffyf-U2qlTnBTJ950gararjRZfKT4mLwiA4sLzwxYQOOX7UX_QZ9TchZ1sB-saxaiqVBJK9eQ8kjKTzFD8kgl8oRv8KDZDWyDlHKSa6BShbwAOIRPEV");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
