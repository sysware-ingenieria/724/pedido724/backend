package com.name.business.businesses;

import com.name.business.DAOs.CommonDAO;
import com.name.business.entities.Common;
import com.name.business.representations.CommonDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.commonStateSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class CommonBusiness {

    private CommonDAO commonDAO;

    public CommonBusiness(CommonDAO commonDAO) {
        this.commonDAO = commonDAO;
    }

    /**
     * @param common
     * @return
     */
    public Either<IException, List<Common>> get(Common common) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Common ||||||||||||||||| ");
            return Either.right(commonDAO.read(commonStateSanitation(common)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(CommonDTO commonDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Common  ||||||||||||||||| ");
            if (commonDTO != null) {

                commonDAO.create(validatorDTO(commonDTO));
                id_common = commonDAO.getPkLast();

                return Either.right(id_common);
            } else {
                msn.add("It does not recognize Common, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_common
     * @param commonDTO
     * @return
     */
    public Either<IException, Long> update(Long id_common, CommonDTO commonDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Common  ||||||||||||||||| ");

            if (commonDTO != null && (id_common != null || id_common > 0)) {
                List<Common> last = commonDAO.read(new Common(id_common, null, null, null));

                if (last.size() <= 0) {
                    msn.add("It does not recognize ID Common , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                commonDAO.update(formatoLongSql(id_common), validatorDTO(id_common, commonDTO));
                return Either.right(id_common);

            } else {
                msn.add("It does not recognize Common , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private CommonDTO validatorDTO(Long id_common, CommonDTO commonDTO) {
        try {
            Common currentCommon = null;
            List<Common> commonList = commonDAO.read(new Common(id_common, null, null, null));
            if (commonList.size() > 0) {
                currentCommon = commonList.get(0);
            }

            // Validate State inserted, if null, set the State to the actual in the database, if not it is formatted to sql
            if (commonDTO.getState() == null || commonDTO.getState() < 0) {
                commonDTO.setState(currentCommon.getState());
            } else {
                commonDTO.setState(formatoIntegerSql(commonDTO.getState()));
            }

            // Validate Creation Date inserted, if null, set the Creation Date to the actual in the database, if not it is formatted to sql
            if (commonDTO.getCreation_date() == null) {
                commonDTO.setCreation_date(currentCommon.getCreation_date());
            } else {
                commonDTO.setCreation_date(formatoDateSql(commonDTO.getCreation_date()));
            }

            if (commonDTO.getModify_date() == null || commonDTO.getModify_date().equals(null)) {
                commonDTO.setModify_date(new Date());
            } else {
                commonDTO.setModify_date(formatoDateSql(commonDTO.getModify_date()));
            }

            return commonDTO;

        } catch (Exception e) {
            return null;
        }

    }

    private CommonDTO validatorDTO(CommonDTO commonDTO) {
        try {

            // Validate State inserted, if null, set the State to the actual in the database, if not it is formatted to sql
            if (commonDTO.getState() == null || commonDTO.getState() < 0) {
                commonDTO.setState(0);
            } else {
                commonDTO.setState(formatoIntegerSql(commonDTO.getState()));
            }

            // Validate Creation Date inserted, if null, set the Creation Date to the actual in the database, if not it is formatted to sql
            if (commonDTO.getCreation_date() == null) {
                commonDTO.setCreation_date(new Date());
            } else {
                commonDTO.setCreation_date(formatoDateSql(commonDTO.getCreation_date()));
            }

            if (commonDTO.getModify_date() == null || commonDTO.getModify_date().equals(null)) {
                commonDTO.setModify_date(new Date());
            } else {
                commonDTO.setModify_date(formatoDateSql(commonDTO.getModify_date()));
            }

            return commonDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        try {
            if (id != 0 && id != null && !id.equals(null)) {
                msn.add("OK");
                System.out.println("|||||||||||| Starting CHANGE STATE DELETE Common ||||||||||||||||| ");
                commonDAO.delete(formatoLongSql(id), new Date());

                return Either.right(id);
            } else {
                msn.add("It does not recognize ID Common, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param idCommon
     * @return
     */
    public Either<IException, Long> permanentDelete(Long idCommon) {
        List<String> msn = new ArrayList<>();
        try {
            if (idCommon != 0 && idCommon != null && !idCommon.equals(null)) {
                msn.add("OK");
                System.out.println("|||||||||||| Starting WARNING DELETE Common in progress ||||||||||||||||| ");
                Common toDelete = new Common(formatoLongSql(idCommon), null, null, null);
                commonDAO.permanentDelete(toDelete);
                return Either.right(idCommon);
            } else {
                msn.add("It does not recognize ID Common, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
