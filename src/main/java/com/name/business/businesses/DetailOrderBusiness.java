package com.name.business.businesses;

import com.name.business.DAOs.DetailOrderDAO;
import com.name.business.entities.Common;
import com.name.business.entities.DetailOrder;
import com.name.business.entities.complete.DetailOrderComplete;
import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.representations.DetailOrderDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.detailOrderCompleteSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class DetailOrderBusiness {

    private DetailOrderDAO detailOrderDAO;
    private OrdersBusiness orderBusiness;
    private CommonBusiness commonBusiness;

    public DetailOrderBusiness(DetailOrderDAO detailOrderDAO, OrdersBusiness orderBusiness, CommonBusiness commonBusiness) {
        this.detailOrderDAO = detailOrderDAO;
        this.orderBusiness = orderBusiness;
        this.commonBusiness = commonBusiness;
    }

    public Either<IException, List<DetailOrderComplete>> get(DetailOrderComplete detailOrderComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults DetailOrder ||||||||||||||||| ");
            return Either.right(detailOrderDAO.read(detailOrderCompleteSanitation(detailOrderComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(DetailOrderDTO detailOrderDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation DetailOrder  ||||||||||||||||| ");
            if (detailOrderDTO != null) {
                detailOrderDTO = validatorDTO(detailOrderDTO);

                Long idCommon = commonBusiness.create(detailOrderDTO.getCommonDTO()).right().value();
                detailOrderDAO.create(detailOrderDTO, idCommon);
                id = detailOrderDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize DetailOrder, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> update(Long id, DetailOrderDTO detailOrderDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_state = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update DetailOrder  ||||||||||||||||| ");

            if (detailOrderDTO != null && (id != null || id > 0)) {
                id = formatoLongSql(id);
                detailOrderDTO = validatorDTO(id, detailOrderDTO);

                List<Long> readCommon = detailOrderDAO.readCommons(formatoLongSql(id));
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID DetailOrder , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonBusiness.update(id_common, detailOrderDTO.getCommonDTO());

                // DetailOrder Update
                detailOrderDAO.update(id, detailOrderDTO);

                return Either.right(formatoLongSql(id));

            } else {
                msn.add("It does not recognize DetailOrder, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private DetailOrderDTO validatorDTO(Long id, DetailOrderDTO detailOrderDTO) {

        DetailOrderComplete detailOrderComplete = null;
        try {

            List<DetailOrderComplete> readLast = detailOrderDAO.read(new DetailOrderComplete(
                    id, null, null, null, null, null,
                    new Common(null, null, null, null)
            ));

            if (readLast.size() > 0) {
                detailOrderComplete = readLast.get(0);
            }

            if (detailOrderDTO.getId_order() == null || detailOrderDTO.getId_order().equals(null)) {
                detailOrderDTO.setId_third(detailOrderComplete.getId_order());
            } else {
                Either<IException, List<OrdersComplete>> readLastOrder = orderBusiness.get(new OrdersComplete(
                        formatoLongSql(detailOrderDTO.getId_order()), null, null,null,
                        new StateOrderComplete(null, null, null, null,
                                new Common(null, null, null, null)),
                        new Common(null, null, null, null)));
                if (readLastOrder.isRight()) {
                    detailOrderDTO.setId_order(formatoLongSql(detailOrderDTO.getId_order()));
                } else {
                    throw new Exception();
                }
            }

            if (detailOrderDTO.getId_product_third() == null || detailOrderDTO.getId_product_third().equals(null)) {
                detailOrderDTO.setId_product_third(detailOrderComplete.getId_product_third());
            } else {
                detailOrderDTO.setId_product_third(formatoLongSql(detailOrderComplete.getId_product_third()));
            }

            if (detailOrderDTO.getQuantity() == null || detailOrderDTO.getQuantity().equals(null)) {
                detailOrderDTO.setQuantity(detailOrderComplete.getQuantity());
            } else {
                detailOrderDTO.setQuantity(formatoIntegerSql(detailOrderDTO.getQuantity()));
            }


            if (detailOrderDTO.getId_third() == null || detailOrderDTO.getId_third().equals(null)) {
                detailOrderDTO.setId_third(detailOrderComplete.getId_third());
            } else {
                detailOrderDTO.setId_third(formatoLongSql(detailOrderDTO.getId_third()));
            }

            if (detailOrderDTO.getValue_product() == null || detailOrderDTO.getValue_product().equals(null)) {
                detailOrderDTO.setValue_product(detailOrderComplete.getValue_product());
            } else {
                detailOrderDTO.setValue_product(formatoLongSql(detailOrderDTO.getValue_product()));
            }

            return detailOrderDTO;

        } catch (Exception e) {
            return null;
        }

    }

    private DetailOrderDTO validatorDTO(DetailOrderDTO detailOrderDTO) {

        try {

            if (detailOrderDTO.getId_order() == null || detailOrderDTO.getId_order().equals(null)) {
                throw new Exception();
            } else {
                Either<IException, List<OrdersComplete>> readLastOrder = orderBusiness.get(new OrdersComplete(
                        (detailOrderDTO.getId_order()), null, null,null,
                        new StateOrderComplete(null, null, null, null,
                                new Common(null, null, null, null)),
                        new Common(null, null, null, null)));
                if (readLastOrder.isRight()) {
                    detailOrderDTO.setId_order(formatoLongSql(detailOrderDTO.getId_order()));
                } else {
                    throw new Exception();
                }
            }

            if (detailOrderDTO.getId_third() == null || detailOrderDTO.getId_third().equals(null)) {
                detailOrderDTO.setId_third(new Long(0));
            } else {
                detailOrderDTO.setId_third(formatoLongSql(detailOrderDTO.getId_third()));
            }


            if (detailOrderDTO.getId_product_third() == null || detailOrderDTO.getId_product_third().equals(null)) {
                throw new Exception();
            } else {
                detailOrderDTO.setId_product_third(formatoLongSql(detailOrderDTO.getId_product_third()));
            }

            if (detailOrderDTO.getQuantity() == null || detailOrderDTO.getQuantity().equals(null)) {
                detailOrderDTO.setQuantity(new Integer(0));
            } else {
                detailOrderDTO.setQuantity(formatoIntegerSql(detailOrderDTO.getQuantity()));
            }

            if (detailOrderDTO.getValue_product() == null || detailOrderDTO.getValue_product().equals(null)) {
                detailOrderDTO.setValue_product(new Long(0));
            } else {
                detailOrderDTO.setValue_product(formatoLongSql(detailOrderDTO.getValue_product()));
            }

            return detailOrderDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE DetailOrder ||||||||||||||||| ");
            List<Long> readCommon = detailOrderDAO.readCommons(formatoLongSql(id));
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID DetailOrder , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE DetailOrder in progress ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = detailOrderDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID DetailOrder, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);

            id = new Long(detailOrderDAO.permanentDelete(new DetailOrder(id, null, null,
                    null, null, null, null)));
            commonBusiness.permanentDelete(idCommon);
            return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
