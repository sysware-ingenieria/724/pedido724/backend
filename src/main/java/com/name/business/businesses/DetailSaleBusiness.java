package com.name.business.businesses;

import com.name.business.DAOs.DetailSaleDAO;
import com.name.business.entities.Common;
import com.name.business.entities.DetailSale;
import com.name.business.entities.complete.DetailSaleComplete;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.representations.DetailSaleDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.detailSaleCompleteSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class DetailSaleBusiness {

    private DetailSaleDAO detailSaleDAO;
    private SaleBusiness saleBusiness;
    private CommonBusiness commonBusiness;

    public DetailSaleBusiness(DetailSaleDAO detailSaleDAO, SaleBusiness saleBusiness, CommonBusiness commonBusiness) {
        this.detailSaleDAO = detailSaleDAO;
        this.saleBusiness = saleBusiness;
        this.commonBusiness = commonBusiness;
    }

    public Either<IException, List<DetailSaleComplete>> get(DetailSaleComplete detailSaleComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults DetailSale ||||||||||||||||| ");
            return Either.right(detailSaleDAO.read(detailSaleCompleteSanitation(detailSaleComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(DetailSaleDTO detailSaleDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation DetailSale  ||||||||||||||||| ");
            if (detailSaleDTO != null) {
                detailSaleDTO = validatorDTO(detailSaleDTO);

                Long idCommon = commonBusiness.create(detailSaleDTO.getCommonDTO()).right().value();
                detailSaleDAO.create(detailSaleDTO, idCommon);
                id = detailSaleDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize DetailSale, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> update(Long id, DetailSaleDTO detailSaleDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_state = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update DetailSale  ||||||||||||||||| ");

            if (detailSaleDTO != null && (id != null || id > 0)) {
                id = formatoLongSql(id);
                detailSaleDTO = validatorDTO(id, detailSaleDTO);

                List<Long> readCommon = detailSaleDAO.readCommons(formatoLongSql(id));
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID DetailSale , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonBusiness.update(id_common, detailSaleDTO.getCommonDTO());

                // DetailSale Update
                detailSaleDAO.update(id, detailSaleDTO);

                return Either.right(formatoLongSql(id));

            } else {
                msn.add("It does not recognize DetailSale, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private DetailSaleDTO validatorDTO(Long id, DetailSaleDTO detailSaleDTO) {

        DetailSaleComplete detailSaleComplete = null;
        try {

            List<DetailSaleComplete> readLast = detailSaleDAO.read(new DetailSaleComplete(
                    id, null, null, null, null, null, null,
                    new Common(null, null, null, null)
            ));

            if (readLast.size() > 0) {
                detailSaleComplete = readLast.get(0);
            }

            if (detailSaleDTO.getId_sale() == null || detailSaleDTO.getId_sale().equals(null)) {
                detailSaleDTO.setId_third(detailSaleComplete.getId_sale());
            } else {
                Either<IException, List<SaleComplete>> readLastSale = saleBusiness.get(new SaleComplete(formatoLongSql(detailSaleDTO.getId_sale()), null, null,
                        null, null, null, null, null, null,
                        new StateOrderComplete(null, null, null, null,
                                new Common(null, null, null, null)),
                        new Common(null, null, null, null)));
                if (readLastSale.isRight()) {
                    detailSaleDTO.setId_sale(formatoLongSql(detailSaleDTO.getId_sale()));
                } else {
                    throw new Exception();
                }
            }

            if (detailSaleDTO.getId_third() == null || detailSaleDTO.getId_third().equals(null)) {
                detailSaleDTO.setId_third(detailSaleComplete.getId_third());
            } else {
                detailSaleDTO.setId_third(formatoLongSql(detailSaleDTO.getId_third()));
            }


            if (detailSaleDTO.getId_product_third() == null || detailSaleDTO.getId_product_third().equals(null)) {
                detailSaleDTO.setId_product_third(detailSaleComplete.getId_product_third());
            } else {
                detailSaleDTO.setId_product_third(formatoLongSql(detailSaleComplete.getId_product_third()));
            }

            if (detailSaleDTO.getOriginal_value() == null || detailSaleDTO.getOriginal_value().equals(null)) {
                detailSaleDTO.setOriginal_value(detailSaleComplete.getOriginal_value());
            } else {
                detailSaleDTO.setOriginal_value(formatoLongSql(detailSaleDTO.getOriginal_value()));
            }

            if (detailSaleDTO.getSale_value() == null || detailSaleDTO.getSale_value().equals(null)) {
                detailSaleDTO.setSale_value(detailSaleComplete.getSale_value());
            } else {
                detailSaleDTO.setSale_value(formatoLongSql(detailSaleDTO.getSale_value()));
            }

            if (detailSaleDTO.getQuantity() == null || detailSaleDTO.getQuantity().equals(null)) {
                detailSaleDTO.setQuantity(detailSaleComplete.getQuantity());
            } else {
                detailSaleDTO.setQuantity(formatoIntegerSql(detailSaleDTO.getQuantity()));
            }

            return detailSaleDTO;

        } catch (Exception e) {
            return null;
        }

    }

    private DetailSaleDTO validatorDTO(DetailSaleDTO detailSaleDTO) {

        try {

            if (detailSaleDTO.getId_sale() == null || detailSaleDTO.getId_sale().equals(null)) {
                throw new Exception();
            } else {
                Either<IException, List<SaleComplete>> readLastSale = saleBusiness.get(new SaleComplete(formatoLongSql(detailSaleDTO.getId_sale()), null, null,
                        null, null, null, null, null, null,
                        new StateOrderComplete(null, null, null, null,
                                new Common(null, null, null, null)),
                        new Common(null, null, null, null)));
                if (readLastSale.isRight()) {
                    detailSaleDTO.setId_sale(formatoLongSql(detailSaleDTO.getId_sale()));
                } else {
                    throw new Exception();
                }
            }

            if (detailSaleDTO.getId_third() == null || detailSaleDTO.getId_third().equals(null)) {
                detailSaleDTO.setId_third(new Long(0));
            } else {
                detailSaleDTO.setId_third(formatoLongSql(detailSaleDTO.getId_third()));
            }


            if (detailSaleDTO.getId_product_third() == null || detailSaleDTO.getId_product_third().equals(null)) {
                throw new Exception();
            } else {
                detailSaleDTO.setId_product_third(formatoLongSql(detailSaleDTO.getId_product_third()));
            }

            if (detailSaleDTO.getOriginal_value() == null || detailSaleDTO.getOriginal_value().equals(null)) {
                detailSaleDTO.setOriginal_value(new Long(0));
            } else {
                detailSaleDTO.setOriginal_value(formatoLongSql(detailSaleDTO.getOriginal_value()));
            }

            if (detailSaleDTO.getSale_value() == null || detailSaleDTO.getSale_value().equals(null)) {
                detailSaleDTO.setSale_value(new Long(0));
            } else {
                detailSaleDTO.setSale_value(formatoLongSql(detailSaleDTO.getSale_value()));
            }

            if (detailSaleDTO.getQuantity() == null || detailSaleDTO.getQuantity().equals(null)) {
                detailSaleDTO.setQuantity(new Integer(0));
            } else {
                detailSaleDTO.setQuantity(formatoIntegerSql(detailSaleDTO.getQuantity()));
            }

            return detailSaleDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE DetailSale ||||||||||||||||| ");
            List<Long> readCommon = detailSaleDAO.readCommons(formatoLongSql(id));
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID DetailSale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE DetailSale in progress ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = detailSaleDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID DetailSale, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }


            idCommon = readCommon.get(0);

            id = new Long(detailSaleDAO.permanentDelete(new DetailSale(id, null, null, null, null, null, null, null)));
            commonBusiness.permanentDelete(idCommon);
            return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
