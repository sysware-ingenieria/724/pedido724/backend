package com.name.business.businesses;

import com.name.business.DAOs.OrdersDAO;
import com.name.business.entities.Common;
import com.name.business.entities.Orders;
import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.representations.OrdersDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.ordersCompleteSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class OrdersBusiness {

    private OrdersDAO orderDAO;
    private SaleBusiness saleBusiness;
    private StateOrderBusiness stateOrdersBusiness;
    private CommonBusiness commonBusiness;

    public OrdersBusiness(OrdersDAO orderDAO, SaleBusiness saleBusiness, StateOrderBusiness stateOrdersBusiness, CommonBusiness commonBusiness) {
        this.orderDAO = orderDAO;
        this.saleBusiness = saleBusiness;
        this.stateOrdersBusiness = stateOrdersBusiness;
        this.commonBusiness = commonBusiness;
    }

    public Either<IException, List<OrdersComplete>> get(OrdersComplete orderComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Orders ||||||||||||||||| ");
            return Either.right(orderDAO.read(ordersCompleteSanitation(orderComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(OrdersDTO orderDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Orders  ||||||||||||||||| ");
            if (orderDTO != null) {
                orderDTO = validatorDTO(orderDTO);

                Long idCommon = commonBusiness.create(orderDTO.getCommonDTO()).right().value();
                Long idState = stateOrdersBusiness.create(orderDTO.getStateOrderDTO()).right().value();
                orderDAO.create(orderDTO, idCommon, idState);
                id = orderDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize Orders, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> update(Long id, OrdersDTO orderDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_state = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Orders  ||||||||||||||||| ");

            if (orderDTO != null && (id != null || id > 0)) {
                id = formatoLongSql(id);
                orderDTO = validatorDTO(id, orderDTO);

                List<Long> readCommon = orderDAO.readCommons(formatoLongSql(id));
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID Orders , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<Long> readState = orderDAO.readState(formatoLongSql(id));
                if (readState.size() <= 0) {
                    msn.add("It does not recognize ID Orders , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonBusiness.update(id_common, orderDTO.getCommonDTO());

                // State Orders Update
                id_state = readState.get(0);
                stateOrdersBusiness.update(id_state, orderDTO.getStateOrderDTO());

                // Orders Update
                orderDAO.update(id, orderDTO);

                return Either.right(formatoLongSql(id));

            } else {
                msn.add("It does not recognize Orders, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private OrdersDTO validatorDTO(Long id, OrdersDTO orderDTO) {

        OrdersComplete orderComplete = null;
        try {

            List<OrdersComplete> readLast = orderDAO.read(new OrdersComplete(
                    id, null, null, null, new StateOrderComplete(null, null, null, null,
                    new Common(null, null, null, null)),
                    new Common(null, null, null, null)
            ));

            if (readLast.size() > 0) {
                orderComplete = readLast.get(0);
            }

            if (orderDTO.getId_third_client() == null || orderDTO.getId_third_client().equals(null)) {
                orderDTO.setId_third_client(orderComplete.getId_third_client());
            } else {
                orderDTO.setId_third_client(formatoLongSql(orderComplete.getId_third_client()));
            }


            if (orderDTO.getId_sale() == null || orderDTO.getId_sale().equals(null)) {
                orderDTO.setId_sale(orderComplete.getId_sale());
            } else {
                Either<IException, List<SaleComplete>> readLastSale = saleBusiness.get(new SaleComplete(formatoLongSql(orderDTO.getId_sale()),null, null,
                        null, null, null, null, null, null,
                        new StateOrderComplete(null, null, null, null,
                                new Common(null, null, null, null)),
                        new Common(null, null, null, null)));
                if(readLastSale.isRight()){
                    orderDTO.setId_sale(formatoLongSql(orderDTO.getId_sale()));
                }
                else{
                    throw new Exception();
                }
            }

            if (orderDTO.getOrder_number() == null || orderDTO.getOrder_number().equals(null)) {
                orderDTO.setOrder_number(orderComplete.getOrder_number());
            } else {
                orderDTO.setOrder_number(formatoLongSql(orderComplete.getOrder_number()));
            }

            return orderDTO;

        } catch (Exception e) {
            return null;
        }

    }

    private OrdersDTO validatorDTO(OrdersDTO orderDTO) {

        try {

            if (orderDTO.getId_third_client() == null || orderDTO.getId_third_client().equals(null)) {
                throw new Exception();
            } else {
                orderDTO.setId_third_client(formatoLongSql(orderDTO.getId_third_client()));
            }


            if (orderDTO.getId_sale() == null || orderDTO.getId_sale().equals(null)) {
                throw new Exception();
            } else {
                Either<IException, List<SaleComplete>> readLastSale = saleBusiness.get(new SaleComplete(formatoLongSql(orderDTO.getId_sale()),null, null,
                        null, null, null, null, null, null,
                        new StateOrderComplete(null, null, null, null,
                                new Common(null, null, null, null)),
                        new Common(null, null, null, null)));
                if(readLastSale.isRight()){
                    orderDTO.setId_sale(formatoLongSql(orderDTO.getId_sale()));
                }
                else{
                    throw new Exception();
                }
            }

            if (orderDTO.getOrder_number() == null || orderDTO.getOrder_number().equals(null)) {
                throw new Exception();
            } else {
                orderDTO.setOrder_number(formatoLongSql(orderDTO.getOrder_number()));
            }

            return orderDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Orders ||||||||||||||||| ");
            List<Long> readCommon = orderDAO.readCommons(formatoLongSql(id));
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID Orders , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        Long idState = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE Orders in progress ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = orderDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID Orders, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            List<Long> readState = orderDAO.readState(id);
            if (readState.size() <= 0) {
                msn.add("It does not recognize ID Orders , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            idState = readState.get(0);

            id = new Long(orderDAO.permanentDelete(new Orders(id, null, null, null, null, null)));
            commonBusiness.permanentDelete(idCommon);
            // stateOrdersBusiness.permanentDelete(idState);
            return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
