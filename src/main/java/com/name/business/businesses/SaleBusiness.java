package com.name.business.businesses;

import com.name.business.DAOs.SaleDAO;
import com.name.business.DAOs.StateOrderDAO;
import com.name.business.entities.Common;
import com.name.business.entities.Sale;
import com.name.business.entities.StateOrder;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.representations.SaleDTO;
import com.name.business.representations.StateOrderDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.saleCompleteSanitation;
import static com.name.business.sanitations.EntitySanitation.stateOrderCompleteSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class SaleBusiness {

    private SaleDAO saleDAO;
    private StateOrderBusiness stateOrderBusiness;
    private CommonBusiness commonBusiness;

    public SaleBusiness(SaleDAO saleDAO, StateOrderBusiness stateOrderBusiness, CommonBusiness commonBusiness) {
        this.saleDAO = saleDAO;
        this.stateOrderBusiness = stateOrderBusiness;
        this.commonBusiness = commonBusiness;
    }

    public Either<IException, List<SaleComplete>> get(SaleComplete saleComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Sale ||||||||||||||||| ");
            return Either.right(saleDAO.read(saleCompleteSanitation(saleComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(SaleDTO saleDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Sale  ||||||||||||||||| ");
            if (saleDTO != null) {
                saleDTO = validatorDTO(saleDTO);

                Long idCommon = commonBusiness.create(saleDTO.getCommonDTO()).right().value();
                Long idState = stateOrderBusiness.create(saleDTO.getStateOrderDTO()).right().value();
                saleDAO.create(saleDTO, idCommon, idState);
                id = saleDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize Sale, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> update(Long id, SaleDTO saleDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_state = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Sale  ||||||||||||||||| ");

            if (saleDTO != null && (id != null || id > 0)) {
                id = formatoLongSql(id);
                saleDTO = validatorDTO(id, saleDTO);

                List<Long> readCommon = saleDAO.readCommons(formatoLongSql(id));
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID Sale , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<Long> readState = saleDAO.readState(formatoLongSql(id));
                if (readState.size() <= 0) {
                    msn.add("It does not recognize ID Sale , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonBusiness.update(id_common, saleDTO.getCommonDTO());

                // State Order Update
                id_state = readState.get(0);
                stateOrderBusiness.update(id_state, saleDTO.getStateOrderDTO());

                // Sale Update
                saleDAO.update(id, saleDTO);

                return Either.right(id);

            } else {
                msn.add("It does not recognize Sale, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private SaleDTO validatorDTO(Long id, SaleDTO saleDTO) {

        SaleComplete saleComplete = null;
        try {

            List<SaleComplete> readLast = saleDAO.read(new SaleComplete(
                    id, null, null, null, null,
                    null, null, null, null,
                    new StateOrderComplete(null, null, null, null,
                            new Common(null, null, null, null)),
                    new Common(null, null, null, null)
            ));

            if (readLast.size() > 0) {
                saleComplete = readLast.get(0);
            }

            if (saleDTO.getId_employee() == null || saleDTO.getId_employee().equals(null)) {
                saleDTO.setId_employee(saleComplete.getId_employee());
            } else {
                saleDTO.setId_employee(formatoLongSql(saleComplete.getId_employee()));
            }

            if (saleDTO.getId_third() == null || saleDTO.getId_third().equals(null)) {
                saleDTO.setId_third(saleComplete.getId_third());
            } else {
                saleDTO.setId_third(formatoLongSql(saleDTO.getId_third()));
            }

            if (saleDTO.getBegin_date() == null || saleDTO.getBegin_date().equals(null)) {
                saleDTO.setBegin_date(saleComplete.getBegin_date());
            } else {
                saleDTO.setBegin_date(formatoDateSql(saleDTO.getBegin_date()));
            }

            if (saleDTO.getFinish_date() == null || saleDTO.getFinish_date().equals(null)) {
                saleDTO.setFinish_date(saleComplete.getFinish_date());
            } else {
                saleDTO.setFinish_date(formatoDateSql(saleDTO.getFinish_date()));
            }

            if (saleDTO.getName_sale() == null || saleDTO.getName_sale().isEmpty()) {
                saleDTO.setName_sale(saleComplete.getName_sale());
            } else {
                saleDTO.setName_sale(formatoStringSql((saleDTO.getName_sale())));
            }

            if (saleDTO.getDescription() == null || saleDTO.getDescription().isEmpty()) {
                saleDTO.setDescription(saleComplete.getDescription_sale());
            } else {
                saleDTO.setDescription(formatoStringSql((saleDTO.getDescription())));
            }

            if (saleDTO.getTotal_value() == null || saleDTO.getTotal_value().equals(null)) {
                saleDTO.setTotal_value(saleComplete.getTotal_value());
            } else {
                saleDTO.setTotal_value(formatoLongSql(saleDTO.getTotal_value()));
            }

            if (saleDTO.getDiscount_percent() == null || saleDTO.getDiscount_percent().equals(null)) {
                saleDTO.setDiscount_percent(saleComplete.getDiscount_percent());
            } else {
                saleDTO.setDiscount_percent(formatoLongSql(saleDTO.getDiscount_percent()));
            }

            return saleDTO;

        } catch (Exception e) {
            return null;
        }

    }

    private SaleDTO validatorDTO(SaleDTO saleDTO) {

        try {

            if (saleDTO.getId_employee() == null || saleDTO.getId_employee().equals(null)) {
                saleDTO.setId_employee(new Long(0));
            } else {
                saleDTO.setId_employee(formatoLongSql(saleDTO.getId_employee()));
            }

            if (saleDTO.getId_third() == null || saleDTO.getId_third().equals(null)) {
                saleDTO.setId_third(new Long(0));
            } else {
                saleDTO.setId_third(formatoLongSql(saleDTO.getId_third()));
            }

            if (saleDTO.getBegin_date() == null || saleDTO.getBegin_date().equals(null)) {
                saleDTO.setBegin_date(new Date());
            } else {
                saleDTO.setBegin_date(formatoDateSql(saleDTO.getBegin_date()));
            }

            if (saleDTO.getFinish_date() == null || saleDTO.getFinish_date().equals(null)) {
                saleDTO.setFinish_date(new Date());
            } else {
                saleDTO.setFinish_date(formatoDateSql(saleDTO.getFinish_date()));
            }

            if (saleDTO.getName_sale() == null || saleDTO.getName_sale().isEmpty()) {
                saleDTO.setName_sale("Sin nombre");
            } else {
                saleDTO.setName_sale(formatoStringSql((saleDTO.getName_sale())));
            }
            
            if (saleDTO.getDescription() == null || saleDTO.getDescription().isEmpty()) {
                saleDTO.setDescription("Sin descripcion");
            } else {
                saleDTO.setDescription(formatoStringSql((saleDTO.getDescription())));
            }

            if (saleDTO.getTotal_value() == null || saleDTO.getTotal_value().equals(null)) {
                throw new Exception();
            } else {
                saleDTO.setTotal_value(formatoLongSql(saleDTO.getTotal_value()));
            }
            
            if (saleDTO.getDiscount_percent() == null || saleDTO.getDiscount_percent().equals(null)) {
                saleDTO.setDiscount_percent(new Long(0));
            } else {
                saleDTO.setDiscount_percent(formatoLongSql(saleDTO.getDiscount_percent()));
            }

            return saleDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Sale ||||||||||||||||| ");
            List<Long> readCommon = saleDAO.readCommons(formatoLongSql(id));
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID Sale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        Long idState = null;

        try {
                msn.add("OK");
                System.out.println("|||||||||||| Starting WARNING DELETE Sale in progress ||||||||||||||||| ");
            id = formatoLongSql(id);
                List<Long> readCommon = saleDAO.readCommons(id);
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID Sale, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            List<Long> readState = saleDAO.readState(id);
            if (readState.size() <= 0) {
                msn.add("It does not recognize ID Sale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

                idCommon = readCommon.get(0);
                idState = readState.get(0);

                id = new Long(saleDAO.permanentDelete(new Sale(id, null, null, null, null,null,null,null,null,null,null)));
                commonBusiness.permanentDelete(idCommon);
                // stateOrderBusiness.permanentDelete(idState);
                return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
