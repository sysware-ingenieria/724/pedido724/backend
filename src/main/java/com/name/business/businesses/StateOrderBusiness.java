package com.name.business.businesses;

import com.name.business.DAOs.StateOrderDAO;
import com.name.business.entities.Common;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.entities.StateOrder;
import com.name.business.representations.StateOrderDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.stateOrderCompleteSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLIKESql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class StateOrderBusiness {

    private StateOrderDAO stateOrderDAO;
    private CommonBusiness commonBusiness;

    public StateOrderBusiness(StateOrderDAO stateOrderDAO, CommonBusiness commonBusiness) {
        this.stateOrderDAO = stateOrderDAO;
        this.commonBusiness = commonBusiness;
    }

    public Either<IException, List<StateOrderComplete>> get(StateOrderComplete stateOrderComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults StateOrder ||||||||||||||||| ");
            return Either.right(stateOrderDAO.read(stateOrderCompleteSanitation(stateOrderComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(StateOrderDTO stateOrderDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation StateOrder  ||||||||||||||||| ");
            if (stateOrderDTO != null) {
                Long idCommon = commonBusiness.create(stateOrderDTO.getCommonDTO()).right().value();
                stateOrderDAO.create(validatorDTO(stateOrderDTO), idCommon);
                id = stateOrderDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize StateOrder, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> update(Long id, StateOrderDTO stateOrderDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update StateOrder  ||||||||||||||||| ");

            if (stateOrderDTO != null && (id != null || id > 0)) {

                List<Long> readCommon = stateOrderDAO.readCommons(formatoLongSql(id));
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID State Order , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonBusiness.update(id_common, stateOrderDTO.getCommonDTO());

                // State Order Update
                stateOrderDAO.update(id, validatorDTO(id_common, stateOrderDTO));

                return Either.right(id);

            } else {
                msn.add("It does not recognize StateOrder , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private StateOrderDTO validatorDTO(Long id, StateOrderDTO stateOrderDTO) {

        StateOrderComplete stateOrderComplete = null;
        try {

            List<StateOrderComplete> readLast = stateOrderDAO.read(new StateOrderComplete(
                    id, null, null, null,
                    new Common(null, null, null, null)
            ));

            if (readLast.size() > 0) {
                stateOrderComplete = readLast.get(0);
            }

            if (stateOrderDTO.getDescription() == null || stateOrderDTO.getDescription().isEmpty()) {
                stateOrderDTO.setDescription(stateOrderComplete.getDescription());
            } else {
                stateOrderDTO.setDescription(formatoLIKESql(stateOrderDTO.getDescription()));
            }

            if (stateOrderDTO.getName_state() == null || stateOrderDTO.getName_state().isEmpty()) {
                stateOrderDTO.setName_state(stateOrderComplete.getName_state());
            } else {
                stateOrderDTO.setName_state(formatoLIKESql(stateOrderDTO.getName_state()));
            }

            if (stateOrderDTO.getType_state() == null || stateOrderDTO.getType_state().isEmpty()) {
                stateOrderDTO.setType_state(stateOrderComplete.getType_state());
            } else {
                stateOrderDTO.setType_state(formatoLIKESql(stateOrderDTO.getType_state()));
            }

            return stateOrderDTO;

        } catch (Exception e) {
            return null;
        }

    }

    private StateOrderDTO validatorDTO(StateOrderDTO stateOrderDTO) {

        try {

            if (stateOrderDTO.getDescription() == null || stateOrderDTO.getDescription().isEmpty()) {
                stateOrderDTO.setDescription("Sin descripcion");
            } else {
                stateOrderDTO.setDescription(formatoStringSql(stateOrderDTO.getDescription()));
            }

            if (stateOrderDTO.getName_state() == null || stateOrderDTO.getName_state().isEmpty()) {
                stateOrderDTO.setName_state("Sin nombre de estado");
            } else {
                stateOrderDTO.setName_state(formatoStringSql(stateOrderDTO.getName_state()));
            }

            if (stateOrderDTO.getType_state() == null || stateOrderDTO.getType_state().isEmpty()) {
                stateOrderDTO.setType_state("Sin tipo de estado");
            } else {
                stateOrderDTO.setType_state(formatoStringSql(stateOrderDTO.getType_state()));
            }

            return stateOrderDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE StateOrder ||||||||||||||||| ");
            List<Long> readCommon = stateOrderDAO.readCommons(formatoLongSql(id));
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID State Order , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;

        try {
                msn.add("OK");
                System.out.println("|||||||||||| Starting WARNING DELETE StateOrder in progress ||||||||||||||||| ");
                List<Long> readCommon = stateOrderDAO.readCommons(formatoLongSql(id));
                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID State Order , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                idCommon = readCommon.get(0);
                id = new Long(stateOrderDAO.permanentDelete(new StateOrder(formatoLongSql(id), null, null, null, null)));
                commonBusiness.permanentDelete(idCommon);
                return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
