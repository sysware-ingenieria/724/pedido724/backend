package com.name.business.businesses.complex;

import com.name.business.businesses.DetailOrderBusiness;
import com.name.business.businesses.OrdersBusiness;
import com.name.business.entities.complete.DetailOrderComplete;
import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.complex.OrderComplex;
import com.name.business.representations.DetailOrderDTO;
import com.name.business.representations.complex.OrdersComplexDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class OrdersComplexBusiness {

    private DetailOrderBusiness detailOrdersBusiness;
    private OrdersBusiness orderBusiness;

    public OrdersComplexBusiness(DetailOrderBusiness detailOrdersBusiness, OrdersBusiness orderBusiness) {
        this.detailOrdersBusiness = detailOrdersBusiness;
        this.orderBusiness = orderBusiness;
    }

    public Either<IException, List<OrderComplex>> get(OrdersComplete orderComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Orders Complex ||||||||||||||||| ");
            List<OrderComplex> get = new ArrayList<>();

            Either<IException, List<OrdersComplete>> getOrders = orderBusiness.get(orderComplete);
            if (getOrders.isRight()) {
                List<OrdersComplete> listOrderss = getOrders.right().value();
                int sizeOrderss = listOrderss.size();
                for (int i = 0; i < sizeOrderss; i++) {
                    Long idOrders = listOrderss.get(i).getId_order();
                    Either<IException, List<DetailOrderComplete>> getDetailOrders =
                            detailOrdersBusiness.get(new DetailOrderComplete(
                                    null,
                                    idOrders,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null
                            ));

                    if (getDetailOrders.isRight()) {
                        List<DetailOrderComplete> listDetailOrderss = getDetailOrders.right().value();
                        get.add(new OrderComplex(listOrderss.get(i), listDetailOrderss));
                    } else {
                        get.add(new OrderComplex(listOrderss.get(i), null));
                    }
                }
            } else {
                return Either.left(getOrders.left().value());
            }

            return Either.right(get);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(OrdersComplexDTO orderComplexDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Orders Complex ||||||||||||||||| ");
            if (orderComplexDTO != null) {

                Either<IException, Long> createOrders = orderBusiness.create(orderComplexDTO.getOrderDTO());

                if (createOrders.isRight()) {
                    List<DetailOrderDTO> listDetailOrdersDTO = orderComplexDTO.getDetailOrderDTO();
                    int sizeDetailOrders = listDetailOrdersDTO.size();
                    for (int i = 0; i < sizeDetailOrders; i++)
                        if (listDetailOrdersDTO.get(i).getId_order().equals(null) ||
                                listDetailOrdersDTO.get(i).getId_order().equals(createOrders.right().value())) {
                            Either<IException, Long> createDetailOrders =
                                    detailOrdersBusiness.create(listDetailOrdersDTO.get(i));

                            if (createDetailOrders.isLeft()) {
                                return Either.left(createDetailOrders.left().value());
                            }
                        }
                }

                id = createOrders.right().value();
                return Either.right(id);
            } else {
                msn.add("It does not recognize Orders Complex, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idOrders = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Orders ||||||||||||||||| ");
            idOrders = formatoLongSql(id);
            Either<IException, Long> deleteOrderss = orderBusiness.delete(idOrders);
            if (deleteOrderss.isRight()) {
                Either<IException, List<DetailOrderComplete>> deleteDetailOrderss =
                        detailOrdersBusiness.get(
                                new DetailOrderComplete(null,
                                        deleteOrderss.right().value(),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null
                                )
                        );

                if (deleteDetailOrderss.isRight()) {
                    List<DetailOrderComplete> listDeleteOrderss = deleteDetailOrderss.right().value();
                    int sizeListDeleteOrderss = listDeleteOrderss.size();
                    for (int i = 0; i < sizeListDeleteOrderss; i++) {
                        Either<IException, Long> deletedDetail =
                                detailOrdersBusiness.delete(listDeleteOrderss.get(i).getId_detail_order());

                        if (deletedDetail.isLeft()) {
                            msn.add("It does not recognize ID Detail Orders " + listDeleteOrderss.get(i).getId_detail_order() + ", probably it has bad format");
                            return Either.left(deletedDetail.left().value());
                        }
                    }
                } else {
                    msn.add("It does not recognize ID Detail Orders , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            } else {
                msn.add("It does not recognize ID Orders , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            return Either.right(idOrders);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idOrders = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE Orders in progress ||||||||||||||||| ");
            idOrders = formatoLongSql(id);
            Either<IException, Long> deleteOrderss = orderBusiness.permanentDelete(idOrders);
            if (deleteOrderss.isRight()) {
                Either<IException, List<DetailOrderComplete>> deleteDetailOrderss =
                        detailOrdersBusiness.get(
                                new DetailOrderComplete(null,
                                        deleteOrderss.right().value(),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null
                                )
                        );

                if (deleteDetailOrderss.isRight()) {
                    List<DetailOrderComplete> listDeleteOrders = deleteDetailOrderss.right().value();
                    int sizeListDeleteOrderss = listDeleteOrders.size();
                    for (int i = 0; i < sizeListDeleteOrderss; i++) {
                        Either<IException, Long> deletedDetail =
                                detailOrdersBusiness.permanentDelete(listDeleteOrders.get(i).getId_detail_order());

                        if (deletedDetail.isLeft()) {
                            msn.add("It does not recognize ID Detail Orders " + listDeleteOrders.get(i).getId_detail_order() + ", probably it has bad format");
                            return Either.left(deletedDetail.left().value());
                        }
                    }
                } else {
                    msn.add("It does not recognize ID Detail Orders , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            } else {
                msn.add("It does not recognize ID Orders , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            return Either.right(idOrders);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
