package com.name.business.businesses.complex;

import com.name.business.businesses.DetailSaleBusiness;
import com.name.business.businesses.SaleBusiness;
import com.name.business.entities.complete.DetailSaleComplete;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complex.SaleComplex;
import com.name.business.representations.DetailSaleDTO;
import com.name.business.representations.complex.SaleComplexDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class SaleComplexBusiness {

    private DetailSaleBusiness detailSaleBusiness;
    private SaleBusiness saleBusiness;

    public SaleComplexBusiness(DetailSaleBusiness detailSaleBusiness, SaleBusiness saleBusiness) {
        this.detailSaleBusiness = detailSaleBusiness;
        this.saleBusiness = saleBusiness;
    }

    public Either<IException, List<SaleComplex>> get(SaleComplete saleComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Sale Complex ||||||||||||||||| ");
            List<SaleComplex> get = new ArrayList<>();

            Either<IException, List<SaleComplete>> getSale = saleBusiness.get(saleComplete);
            if (getSale.isRight()) {
                List<SaleComplete> listSales = getSale.right().value();
                int sizeSales = listSales.size();
                for (int i = 0; i < sizeSales; i++) {
                    Long idSales = listSales.get(i).getId_sale();
                    Either<IException, List<DetailSaleComplete>> getDetailSale =
                            detailSaleBusiness.get(new DetailSaleComplete(
                                    null,
                                    idSales,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null
                            ));

                    if (getDetailSale.isRight()) {
                        List<DetailSaleComplete> listDetailSales = getDetailSale.right().value();
                        get.add(new SaleComplex(listSales.get(i), listDetailSales));
                    } else {
                        get.add(new SaleComplex(listSales.get(i), null));
                    }
                }
            } else {
                return Either.left(getSale.left().value());
            }

            return Either.right(get);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(SaleComplexDTO saleComplexDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Sale Complex ||||||||||||||||| ");
            if (saleComplexDTO != null) {

                Either<IException, Long> createSale = saleBusiness.create(saleComplexDTO.getSaleDTO());

                if (createSale.isRight()) {
                    List<DetailSaleDTO> listDetailSaleDTO = saleComplexDTO.getDetailSaleDTO();
                    int sizeDetailSale = listDetailSaleDTO.size();
                    for (int i = 0; i < sizeDetailSale; i++)
                        if (listDetailSaleDTO.get(i).getId_sale().equals(null) ||
                                listDetailSaleDTO.get(i).getId_sale().equals(createSale.right().value())) {
                            Either<IException, Long> createDetailSale =
                                    detailSaleBusiness.create(listDetailSaleDTO.get(i));

                            if (createDetailSale.isLeft()) {
                                return Either.left(createDetailSale.left().value());
                            }
                        }
                }

                id = createSale.right().value();
                return Either.right(id);
            } else {
                msn.add("It does not recognize Sale Complex, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idSale = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Sale ||||||||||||||||| ");
            idSale = formatoLongSql(id);
            Either<IException, Long> deleteSales = saleBusiness.delete(idSale);
            if (deleteSales.isRight()) {
                Either<IException, List<DetailSaleComplete>> deleteDetailSales =
                        detailSaleBusiness.get(
                                new DetailSaleComplete(null,
                                        deleteSales.right().value(),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null
                                )
                        );

                if (deleteDetailSales.isRight()) {
                    List<DetailSaleComplete> listDeleteSales = deleteDetailSales.right().value();
                    int sizeListDeleteSales = listDeleteSales.size();
                    for (int i = 0; i < sizeListDeleteSales; i++) {
                        Either<IException, Long> deletedDetail =
                                detailSaleBusiness.delete(listDeleteSales.get(i).getId_detail_sale());

                        if (deletedDetail.isLeft()) {
                            msn.add("It does not recognize ID Detail Sale " + listDeleteSales.get(i).getId_detail_sale() + ", probably it has bad format");
                            return Either.left(deletedDetail.left().value());
                        }
                    }
                } else {
                    msn.add("It does not recognize ID Detail Sale , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            } else {
                msn.add("It does not recognize ID Sale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            return Either.right(idSale);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idSale = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE Sale in progress ||||||||||||||||| ");
            idSale = formatoLongSql(id);
            Either<IException, Long> deleteSales = saleBusiness.permanentDelete(idSale);
            if (deleteSales.isRight()) {
                Either<IException, List<DetailSaleComplete>> deleteDetailSales =
                        detailSaleBusiness.get(
                                new DetailSaleComplete(null,
                                        deleteSales.right().value(),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null
                                )
                        );

                if (deleteDetailSales.isRight()) {
                    List<DetailSaleComplete> listDeleteSales = deleteDetailSales.right().value();
                    int sizeListDeleteSales = listDeleteSales.size();
                    for (int i = 0; i < sizeListDeleteSales; i++) {
                        Either<IException, Long> deletedDetail =
                                detailSaleBusiness.permanentDelete(listDeleteSales.get(i).getId_detail_sale());

                        if (deletedDetail.isLeft()) {
                            msn.add("It does not recognize ID Detail Sale " + listDeleteSales.get(i).getId_detail_sale() + ", probably it has bad format");
                            return Either.left(deletedDetail.left().value());
                        }
                    }
                } else {
                    msn.add("It does not recognize ID Detail Sale , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            } else {
                msn.add("It does not recognize ID Sale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            return Either.right(idSale);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
