package com.name.business.businesses.notification;

import com.name.business.DAOs.Notification.CommonNotificationDAO;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.representations.notification.CommonNotificationDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.commonNotificationSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoDateSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class CommonNotificationBusiness {

    private CommonNotificationDAO commonNotificationDAO;

    public CommonNotificationBusiness(CommonNotificationDAO commonNotificationDAO) {
        this.commonNotificationDAO = commonNotificationDAO;
    }

    /**
     * @param
     * @return
     */
    public Either<IException, List<CommonNotification>> get(CommonNotification commonNotification) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Common ||||||||||||||||| ");
            return Either.right(commonNotificationDAO.read(commonNotificationSanitation(commonNotification)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(CommonNotificationDTO commonNotificationDTO) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation CommonNotification  ||||||||||||||||| ");
            if (commonNotificationDTO != null) {
                CommonNotificationDTO createDTO = validatorDTO(commonNotificationDTO);
                commonNotificationDAO.create(createDTO);
                id_common = commonNotificationDAO.getPkLast();
                return Either.right(id_common);
            } else {
                msn.add("It does not recognize CommonNotification, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_common
     * @param commonNotificationDTO
     * @return
     */
    public Either<IException, Long> update(Long id_common, CommonNotificationDTO commonNotificationDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update CommonNotification  ||||||||||||||||| ");

            if (commonNotificationDTO != null && (id_common != null || id_common > 0)) {
                List<CommonNotification> last = commonNotificationDAO.read(new CommonNotification(id_common, null, null, null));

                if (last.size() <= 0) {
                    msn.add("It does not recognize ID Common , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                commonNotificationDAO.update(formatoLongSql(id_common), validatorDTO(id_common, commonNotificationDTO));
                return Either.right(id_common);

            } else {
                msn.add("It does not recognize CommonNotification , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private CommonNotificationDTO validatorDTO(Long id_common, CommonNotificationDTO commonNotificationDTO) {
        try {
            CommonNotification currentCommon = null;
            List<CommonNotification> commonList = commonNotificationDAO.read(new CommonNotification(id_common, null, null, null));
            if (commonList.size() > 0) {
                currentCommon = commonList.get(0);
            }

            // Validate State inserted, if null, set the State to the actual in the database, if not it is formatted to sql
            if (commonNotificationDTO.getState() == null || commonNotificationDTO.getState() < 0) {
                commonNotificationDTO.setState(currentCommon.getState());
            } else {
                commonNotificationDTO.setState(formatoIntegerSql(commonNotificationDTO.getState()));
            }

            // Validate Creation Date inserted, if null, set the Creation Date to the actual in the database, if not it is formatted to sql
            if (commonNotificationDTO.getCreation_date() == null) {
                commonNotificationDTO.setCreation_date(currentCommon.getCreation_date());
            }

            return commonNotificationDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public static CommonNotificationDTO validatorDTO(CommonNotificationDTO commonDTO) {
        try {

            // Validate State inserted, if null, set the State to the actual in the database, if not it is formatted to sql
            if (commonDTO.getState() == null || commonDTO.getState() < 0) {
                commonDTO.setState(0);
            } else {
                commonDTO.setState(formatoIntegerSql(commonDTO.getState()));
            }

            // Validate Creation Date inserted, if null, set the Creation Date to the actual in the database, if not it is formatted to sql
            if (commonDTO.getCreation_date() == null) {
                commonDTO.setCreation_date(new Date());
            } else {
                commonDTO.setCreation_date(formatoDateSql(commonDTO.getCreation_date()));
            }

            if (commonDTO.getModify_date() == null || commonDTO.getModify_date().equals(null)) {
                commonDTO.setModify_date(new Date());
            } else {
                commonDTO.setModify_date(formatoDateSql(commonDTO.getModify_date()));
            }

            return commonDTO;

        } catch (Exception e) {
            return null;
        }

    }

    /**
     * @param idCommon
     * @return
     */
    public Either<IException, Long> delete(Long idCommon) {
        List<String> msn = new ArrayList<>();
        try {
            if (idCommon != 0) {
                msn.add("OK");
                System.out.println("|||||||||||| Starting CHANGE STATE DELETE CommonNotification ||||||||||||||||| ");
                commonNotificationDAO.delete(formatoLongSql(idCommon), new Date());

                return Either.right(idCommon);
            } else {
                msn.add("It does not recognize ID CommonNotification, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param idCommon
     * @return
     */
    public Either<IException, Long> permanentDelete(Long idCommon) {
        List<String> msn = new ArrayList<>();
        try {
            if (idCommon != 0) {
                msn.add("OK");
                System.out.println("|||||||||||| Starting WARNING DELETE CommonNotification in progress ||||||||||||||||| ");
                CommonNotification toDelete = new CommonNotification(formatoLongSql(idCommon), null, null, null);
                commonNotificationDAO.permanentDelete(toDelete);
                return Either.right(idCommon);
            } else {
                msn.add("It does not recognize ID CommonNotification, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
