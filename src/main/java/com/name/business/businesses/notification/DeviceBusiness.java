package com.name.business.businesses.notification;

import com.name.business.DAOs.Notification.DeviceDAO;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.Device;
import com.name.business.entities.notification.complete.DeviceComplete;
import com.name.business.representations.notification.DeviceDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.deviceSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class DeviceBusiness {

    private DeviceDAO deviceDAO;
    private CommonNotificationBusiness commonNotificationBusiness;

    public DeviceBusiness(DeviceDAO devicedao, CommonNotificationBusiness commonNotificationBusiness) {
        this.deviceDAO = devicedao;
        this.commonNotificationBusiness = commonNotificationBusiness;
    }

    public Either<IException, List<DeviceComplete>> get(DeviceComplete deviceComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Device ||||||||||||||||| ");
            return Either.right(deviceDAO.read(deviceSanitation(deviceComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(DeviceDTO deviceDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Device  ||||||||||||||||| ");
            if (deviceDTO != null) {
                deviceDTO = validatorDTO(deviceDTO);

                Long idCommon = commonNotificationBusiness.create(deviceDTO.getCommonDTO()).right().value();
                deviceDAO.create(deviceDTO, idCommon);
                id = deviceDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize Device, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> update(Long id, DeviceDTO devicedto) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update CommonNotification  ||||||||||||||||| ");

            if (devicedto != null && (id != null || id > 0)) {
                id = formatoLongSql(id);
                devicedto = validatorDTO(id, devicedto);


                List<Long> readCommon = deviceDAO.readCommons(id);

                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID deviceDTO , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonNotificationBusiness.update(id_common, devicedto.getCommonDTO());

                //Device Update
                deviceDAO.update(id, devicedto);
                return Either.right(id);


            } else {
                msn.add("It does not recognize Device , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public DeviceDTO validatorDTO(Long id, DeviceDTO deviceDTO) {
        DeviceComplete deviceComplete = null;
        try {

            List<DeviceComplete> readLast = deviceDAO.read(new DeviceComplete(
                    id, null, null, null,
                    new CommonNotification(
                            null, null,
                            null, null
                    )
            ));

            if (readLast.size() > 0) {
                deviceComplete = readLast.get(0);
            }

            if (deviceDTO.getDevice_number() == null || deviceDTO.getDevice_number().equals(null)) {
                deviceDTO.setDevice_number(deviceComplete.getDevice_number());
            } else {
                deviceDTO.setDevice_number(formatoLongSql(deviceDTO.getDevice_number()));
            }

            if (deviceDTO.getUuid() == null || deviceDTO.getUuid().equals(null)) {
                deviceDTO.setUuid(deviceComplete.getUuid());
            } else {
                deviceDTO.setUuid(formatoLongSql(deviceDTO.getUuid()));
            }

            if (deviceDTO.getDevice_type() == null || deviceDTO.getDevice_type().equals(null)) {
                deviceDTO.setDevice_type(deviceComplete.getDevice_type());
            } else {
                deviceDTO.setDevice_type(formatoStringSql(deviceDTO.getDevice_type()));
            }
            
            return deviceDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public static DeviceDTO validatorDTO(DeviceDTO deviceDTO) {
        try {

            if (deviceDTO.getDevice_number() == null || deviceDTO.getDevice_number().equals(null)) {
                //deviceDTO.setDevice_number(new String(""));
                throw new Exception();
            } else {
                deviceDTO.setDevice_number(formatoLongSql(deviceDTO.getDevice_number()));
            }

            if (deviceDTO.getUuid() == null || deviceDTO.getUuid().equals(null)) {
                //deviceDTO.setUuid(new String(""));
                throw new Exception();
            } else {
                deviceDTO.setUuid(formatoLongSql(deviceDTO.getUuid()));
            }

            if (deviceDTO.getDevice_type() == null || deviceDTO.getDevice_type().equals(null)) {
                //deviceDTO.setDevice_type(new String(""));
                throw new Exception();
            } else {
                deviceDTO.setDevice_type(formatoStringSql(deviceDTO.getDevice_type()));
            }

            return deviceDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Sale ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = deviceDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID Sale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonNotificationBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE Device in progress ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = deviceDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID Device, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);

            id = new Long(deviceDAO.permanentDelete(new Device(id, null, null, null, null)));
            commonNotificationBusiness.permanentDelete(idCommon);
            return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
