package com.name.business.businesses.notification;

import com.name.business.DAOs.Notification.FCMDAO;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.FCM;
import com.name.business.entities.notification.complete.FCMComplete;
import com.name.business.representations.notification.CommonNotificationDTO;
import com.name.business.representations.notification.FCMDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.fcmSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class FCMBusiness {

    private FCMDAO fcmDAO;
    private CommonNotificationBusiness commonNotificationBusiness;

    public FCMBusiness(FCMDAO fcmdao, CommonNotificationBusiness commonNotificationBusiness) {
        this.fcmDAO = fcmdao;
        this.commonNotificationBusiness = commonNotificationBusiness;
    }

    /**
     * @param
     * @return
     */
    public Either<IException, List<FCMComplete>> get(FCMComplete fcmComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults FCM ||||||||||||||||| ");
            return Either.right(fcmDAO.read(fcmSanitation(fcmComplete)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> create(FCMDTO fcmDTO) {
        List<String> msn = new ArrayList<>();
        Long id = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation FCM  ||||||||||||||||| ");
            if (fcmDTO != null) {
                fcmDTO = validatorDTO(fcmDTO);

                Long idCommon = commonNotificationBusiness.create(fcmDTO.getCommonNotificationDTO()).right().value();
                fcmDAO.create(fcmDTO, idCommon);
                id = fcmDAO.getPkLast();
                return Either.right(id);
            } else {
                msn.add("It does not recognize FCM, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> update(Long id, FCMDTO fcmdto) {
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update CommonNotification  ||||||||||||||||| ");

            if (fcmdto != null && (id != null || id > 0)) {
                id = formatoLongSql(id);
                fcmdto = validatorDTO(id, fcmdto);


                List<Long> readCommon = fcmDAO.readCommons(id);

                if (readCommon.size() <= 0) {
                    msn.add("It does not recognize ID fcmDTO , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                // Common update
                id_common = readCommon.get(0);
                commonNotificationBusiness.update(id_common,fcmdto.getCommonNotificationDTO());

                //FCM Update
                fcmDAO.update(id, fcmdto);
                return Either.right(id);


            } else {
                msn.add("It does not recognize FCM , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public FCMDTO validatorDTO(Long id, FCMDTO fcmDTO) {
        FCMComplete fcmComplete = null;
        try {

            List<FCMComplete> readLast = fcmDAO.read(new FCMComplete(
                    id, null, null, null,
                    null, null, null,
                    new CommonNotification(
                            null, null,
                            null, null
                    )
            ));

            if(readLast.size() > 0){
                fcmComplete = readLast.get(0);
            }


            if (fcmDTO.getTitle() == null || fcmDTO.getTitle().equals(null)) {
                fcmDTO.setTitle(fcmComplete.getTitle());
            } else {
                fcmDTO.setTitle(formatoStringSql(fcmDTO.getTitle()));
            }

            if (fcmDTO.getDescription() == null || fcmDTO.getDescription().equals(null)) {
                fcmDTO.setDescription(fcmComplete.getDescription());
            } else {
                fcmDTO.setDescription(formatoStringSql(fcmDTO.getDescription()));
            }

            if (fcmDTO.getData_info() == null || fcmDTO.getData_info().equals(null)) {
                fcmDTO.setData_info(fcmComplete.getData_info());
            } else {
                fcmDTO.setData_info(formatoStringSql(fcmDTO.getData_info()));
            }

            if (fcmDTO.getState_fcm() == null || fcmDTO.getState_fcm().equals(null)) {
                fcmDTO.setState_fcm(fcmComplete.getState_fcm());
            } else {
                fcmDTO.setState_fcm(formatoStringSql(fcmDTO.getState_fcm()));
            }

            if (fcmDTO.getDate_sent() == null || fcmDTO.getDate_sent().equals(null)) {
                fcmDTO.setDate_sent(new Timestamp((new Date()).getTime()));
            } else {
                fcmDTO.setDate_sent(new Timestamp(formatoDateSql(fcmDTO.getDate_sent()).getTime()));
            }

            if (fcmDTO.getData_info() == null || fcmDTO.getData_info().equals(null)) {
                fcmDTO.setData_info(fcmComplete.getData_info());
                throw new Exception();
            } else {
                fcmDTO.setData_info(formatoStringSql(fcmDTO.getData_info()));
            }

            return fcmDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public static FCMDTO validatorDTO(FCMDTO fcmDTO) {
        try {

            if (fcmDTO.getTitle() == null || fcmDTO.getTitle().equals(null)) {
                //fcmDTO.setTitle(new String(""));
                throw new Exception();
            } else {
                fcmDTO.setTitle(formatoStringSql(fcmDTO.getTitle()));
            }

            if (fcmDTO.getDescription() == null || fcmDTO.getDescription().equals(null)) {
                //fcmDTO.setDescription(new String(""));
                throw new Exception();
            } else {
                fcmDTO.setDescription(formatoStringSql(fcmDTO.getDescription()));
            }

            if (fcmDTO.getData_info() == null || fcmDTO.getData_info().equals(null)) {
                //fcmDTO.setData_info(new String(""));
                throw new Exception();
            } else {
                fcmDTO.setData_info(formatoStringSql(fcmDTO.getData_info()));
            }

            if (fcmDTO.getState_fcm() == null || fcmDTO.getState_fcm().equals(null)) {
                //fcmDTO.setState_fcm(new String(""));
                throw new Exception();
            } else {
                fcmDTO.setState_fcm(formatoStringSql(fcmDTO.getState_fcm()));
            }

            if (fcmDTO.getDate_sent() == null || fcmDTO.getDate_sent().equals(null)) {
                fcmDTO.setDate_sent(new Timestamp((new Date()).getTime()));
            } else {
                fcmDTO.setDate_sent(new Timestamp(formatoDateSql(fcmDTO.getDate_sent()).getTime()));
            }

            if (fcmDTO.getData_info() == null || fcmDTO.getData_info().equals(null)) {
                //fcmDTO.setData_info(new String(""));
                throw new Exception();
            } else {
                fcmDTO.setData_info(formatoStringSql(fcmDTO.getData_info()));
            }

            return fcmDTO;

        } catch (Exception e) {
            return null;
        }

    }

    public Either<IException, Long> delete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;
        try {

            msn.add("OK");
            System.out.println("|||||||||||| Starting CHANGE STATE DELETE Sale ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = fcmDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID Sale , probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);
            commonNotificationBusiness.delete(idCommon);
            return Either.right(id);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> permanentDelete(Long id) {
        List<String> msn = new ArrayList<>();
        Long idCommon = null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting WARNING DELETE FCM in progress ||||||||||||||||| ");
            id = formatoLongSql(id);
            List<Long> readCommon = fcmDAO.readCommons(id);
            if (readCommon.size() <= 0) {
                msn.add("It does not recognize ID FCM, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            idCommon = readCommon.get(0);

            id = new Long(fcmDAO.permanentDelete(new FCM(id, null, null, null, null,null,null,null)));
            commonNotificationBusiness.permanentDelete(idCommon);
            return Either.right(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
