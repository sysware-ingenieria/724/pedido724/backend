package com.name.business.entities;

public class DetailOrder {
    private Long id_detail_order;
    private Long id_order;
    private Long id_product_third;
    private Integer quantity;
    private Long id_third;
    private Long value_product;
    private Long id_common;

    public DetailOrder(Long id_detail_order, Long id_order, Long id_product_third, Integer quantity, Long id_third, Long value_product, Long id_common) {
        this.id_detail_order = id_detail_order;
        this.id_order = id_order;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.id_third = id_third;
        this.value_product = value_product;
        this.id_common = id_common;
    }

    public Long getId_detail_order() {
        return id_detail_order;
    }

    public void setId_detail_order(Long id_detail_order) {
        this.id_detail_order = id_detail_order;
    }

    public Long getId_order() {
        return id_order;
    }

    public void setId_order(Long id_order) {
        this.id_order = id_order;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getValue_product() {
        return value_product;
    }

    public void setValue_product(Long value_product) {
        this.value_product = value_product;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }
}
