package com.name.business.entities;

public class DetailSale {
    private Long id_detail_sale;
    private Long id_sale;
    private Long id_common;
    private Long id_third;
    private Long id_product_third;
    private Long original_value;
    private Long sale_value;
    private Integer quantity;

    public DetailSale(Long id_detail_sale, Long id_sale, Long id_common, Long id_third, Long id_product_third, Long original_value, Long sale_value, Integer quantity) {
        this.id_detail_sale = id_detail_sale;
        this.id_sale = id_sale;
        this.id_common = id_common;
        this.id_third = id_third;
        this.id_product_third = id_product_third;
        this.original_value = original_value;
        this.sale_value = sale_value;
        this.quantity = quantity;
    }

    public Long getId_detail_sale() {
        return id_detail_sale;
    }

    public void setId_detail_sale(Long id_detail_sale) {
        this.id_detail_sale = id_detail_sale;
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Long getOriginal_value() {
        return original_value;
    }

    public void setOriginal_value(Long original_value) {
        this.original_value = original_value;
    }

    public Long getSale_value() {
        return sale_value;
    }

    public void setSale_value(Long sale_value) {
        this.sale_value = sale_value;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
