package com.name.business.entities;

public class Orders {
    private Long id_order;
    private Long id_third_client;
    private Long id_state;
    private Long order_number;
    private Long id_sale;
    private Long id_common;

    public Orders(Long id_order, Long id_third_client, Long id_state, Long order_number, Long id_sale, Long id_common) {
        this.id_order = id_order;
        this.id_third_client = id_third_client;
        this.id_state = id_state;
        this.order_number = order_number;
        this.id_sale = id_sale;
        this.id_common = id_common;
    }

    public Long getId_order() {
        return id_order;
    }

    public void setId_order(Long id_order) {
        this.id_order = id_order;
    }

    public Long getId_third_client() {
        return id_third_client;
    }

    public void setId_third_client(Long id_third_client) {
        this.id_third_client = id_third_client;
    }

    public Long getId_state() {
        return id_state;
    }

    public void setId_state(Long id_state) {
        this.id_state = id_state;
    }

    public Long getOrder_number() {
        return order_number;
    }

    public void setOrder_number(Long order_number) {
        this.order_number = order_number;
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }
}
