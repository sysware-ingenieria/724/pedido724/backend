package com.name.business.entities;

import java.util.Date;

public class Sale {
    private Long id_sale;
    private Long id_employee;
    private Long id_third;
    private Long id_common;
    private Long id_state;
    private Date begin_date;
    private Date finish_date;
    private String name_sale;
    private String description;
    private Long total_value;
    private Long discount_percent;

    public Sale(Long id_sale, Long id_employee, Long id_third, Long id_common, Long id_state, Date begin_date, Date finish_date, String name_sale, String description, Long total_value, Long discount_percent) {
        this.id_sale = id_sale;
        this.id_employee = id_employee;
        this.id_third = id_third;
        this.id_common = id_common;
        this.id_state = id_state;
        this.begin_date = begin_date;
        this.finish_date = finish_date;
        this.name_sale = name_sale;
        this.description = description;
        this.total_value = total_value;
        this.discount_percent = discount_percent;
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public Long getId_employee() {
        return id_employee;
    }

    public void setId_employee(Long id_employee) {
        this.id_employee = id_employee;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Long getId_state() {
        return id_state;
    }

    public void setId_state(Long id_state) {
        this.id_state = id_state;
    }

    public Date getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(Date begin_date) {
        this.begin_date = begin_date;
    }

    public Date getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(Date finish_date) {
        this.finish_date = finish_date;
    }

    public String getName_sale() {
        return name_sale;
    }

    public void setName_sale(String name_sale) {
        this.name_sale = name_sale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTotal_value() {
        return total_value;
    }

    public void setTotal_value(Long total_value) {
        this.total_value = total_value;
    }

    public Long getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(Long discount_percent) {
        this.discount_percent = discount_percent;
    }
}
