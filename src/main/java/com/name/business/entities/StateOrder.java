package com.name.business.entities;

public class StateOrder {
    private Long id_state;
    private String name_state;
    private String description;
    private String type_state;
    private Long id_common;

    public StateOrder(Long id_state, String name_state, String description, String type_state, Long id_common) {
        this.id_state = id_state;
        this.name_state = name_state;
        this.description = description;
        this.type_state = type_state;
        this.id_common = id_common;
    }

    public Long getId_state() {
        return id_state;
    }

    public void setId_state(Long id_state) {
        this.id_state = id_state;
    }

    public String getName_state() {
        return name_state;
    }

    public void setName_state(String name_state) {
        this.name_state = name_state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType_state() {
        return type_state;
    }

    public void setType_state(String type_state) {
        this.type_state = type_state;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }
}
