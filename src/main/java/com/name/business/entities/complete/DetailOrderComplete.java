package com.name.business.entities.complete;

import com.name.business.entities.Common;

import java.util.Date;

/***
 * @TODO
 * Resolver los deprecated por los
 * tipos de datods correspondientes.
 */
public class DetailOrderComplete {

    private Long id_detail_order;
    private Long id_order;
    private Long id_product_third;
    private Integer quantity;
    private Long id_third;
    @Deprecated
    private Long value_product;
    private Long id_common;
    private Integer common_state;
    private Date creation_date;
    private Date modify_date;

    public DetailOrderComplete(Long id_detail_order, Long id_order, Long id_product_third, Integer quantity, Long id_third, Long value_product, Common common
    ) {
        this.id_detail_order = id_detail_order;
        this.id_order = id_order;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.id_third = id_third;
        this.value_product = value_product;
        this.id_common = common.getId_common_state();
        this.common_state = common.getState();
        this.creation_date = common.getCreation_date();
        this.modify_date = common.getModify_date();
    }

    public Common getCommon(){
        return new Common(
                this.id_common,
                this.common_state,
                this.creation_date,
                this.modify_date
        );
    }

    public void setCommon(Common commonNotification){
        this.id_common = commonNotification.getId_common_state();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public Long getId_detail_order() {
        return id_detail_order;
    }

    public void setId_detail_order(Long id_detail_order) {
        this.id_detail_order = id_detail_order;
    }

    public Long getId_order() {
        return id_order;
    }

    public void setId_order(Long id_order) {
        this.id_order = id_order;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getValue_product() {
        return value_product;
    }

    public void setValue_product(Long value_product) {
        this.value_product = value_product;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Integer getState() {
        return common_state;
    }

    public void setState(Integer common_state) {
        this.common_state = common_state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
