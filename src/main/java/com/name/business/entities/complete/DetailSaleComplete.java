package com.name.business.entities.complete;

import com.name.business.entities.Common;

import java.util.Date;


/**
 * TODO
 * Cambiar tipos de datos para:
 * @original_value
 * @sale_value
 */
public class DetailSaleComplete {

    private Long id_detail_sale;
    private Long id_sale;
    private Long id_third;
    private Long id_product_third;

    @Deprecated
    private Long original_value;
    @Deprecated
    private Long sale_value;

    private Integer quantity;
    private Long id_common;
    private Integer common_state;
    private Date creation_date;
    private Date modify_date;

    public DetailSaleComplete(Long id_detail_sale, Long id_sale, Long id_third, Long id_product_third, Long original_value, Long sale_value, Integer quantity, Common common) {
        this.id_detail_sale = id_detail_sale;
        this.id_sale = id_sale;
        this.id_third = id_third;
        this.id_product_third = id_product_third;
        this.original_value = original_value;
        this.sale_value = sale_value;
        this.quantity = quantity;
        this.id_common = common.getId_common_state();
        this.common_state = common.getState();
        this.creation_date = common.getCreation_date();
        this.modify_date = common.getModify_date();
    }

    public Common getCommon(){
        return new Common(
                this.id_common,
                this.common_state,
                this.creation_date,
                this.modify_date
        );
    }

    public void setCommon(Common commonNotification){
        this.id_common = commonNotification.getId_common_state();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public Long getId_detail_sale() {
        return id_detail_sale;
    }

    public void setId_detail_sale(Long id_detail_sale) {
        this.id_detail_sale = id_detail_sale;
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Long getOriginal_value() {
        return original_value;
    }

    public void setOriginal_value(Long original_value) {
        this.original_value = original_value;
    }

    public Long getSale_value() {
        return sale_value;
    }

    public void setSale_value(Long sale_value) {
        this.sale_value = sale_value;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Integer getCommon_state() {
        return common_state;
    }

    public void setCommon_state(Integer common_state) {
        this.common_state = common_state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
