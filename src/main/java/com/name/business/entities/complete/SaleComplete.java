package com.name.business.entities.complete;

import com.name.business.entities.Common;

import java.util.Date;

/**
 *@TODO
 * Resolver los deprecated, problemas de tipado de datos
 * */

public class SaleComplete {

    private Long id_sale;
    private Long id_employee;
    private Long id_third;
    private Date begin_date;
    private Date finish_date;
    private String name_sale;
    private String description_sale;

    @Deprecated
    private Long total_value;
    @Deprecated
    private Long discount_percent;

    private Long id_state;
    private String name_state;
    private String description_state;
    private String type_state;
    private Long id_common_state;
    private Integer state_state;
    private Date creation_date_state;
    private Date modify_date_state;

    private Long id_common;
    private Integer common_state;
    private Date creation_date;
    private Date modify_date;

    public SaleComplete(Long id_sale, Long id_employee, Long id_third, Date begin_date, Date finish_date, String name_sale, String description_sale, Long total_value, Long discount_percent, StateOrderComplete stateOrder, Common common) {
        this.id_sale = id_sale;
        this.id_employee = id_employee;
        this.id_third = id_third;
        this.begin_date = begin_date;
        this.finish_date = finish_date;
        this.name_sale = name_sale;
        this.description_sale = description_sale;
        this.total_value = total_value;
        this.discount_percent = discount_percent;

        this.id_state = stateOrder.getId_state();
        this.name_state = stateOrder.getName_state();
        this.description_state = stateOrder.getDescription();
        this.type_state = stateOrder.getType_state();
        this.id_common_state = stateOrder.getId_common();
        this.state_state = stateOrder.getCommon_state();
        this.creation_date_state = stateOrder.getCreation_date();
        this.modify_date_state = stateOrder.getModify_date();

        this.id_common = common.getId_common_state();
        this.common_state = common.getState();
        this.creation_date = common.getCreation_date();
        this.modify_date = common.getModify_date();
    }

    public StateOrderComplete getStateOrderComplete() {
        return new StateOrderComplete(
                this.id_state,
                this.name_state,
                this.description_state,
                this.type_state,
                new Common(this.id_common_state,
                        this.state_state,
                        this.creation_date_state,
                        this.modify_date_state)
        );
    }
    
    public void setStateOrderComplete(StateOrderComplete stateOrderComplete){
        this.id_state = stateOrderComplete.getId_state();
        this.name_state = stateOrderComplete.getName_state();
        this.description_state = stateOrderComplete.getDescription();
        this.type_state = stateOrderComplete.getType_state();
        this.id_common_state = stateOrderComplete.getId_common();
        this.state_state = stateOrderComplete.getCommon_state();
        this.creation_date_state = stateOrderComplete.getCreation_date();
        this.modify_date_state = stateOrderComplete.getModify_date();
    }

    public Common getCommon() {
        return new Common(
                this.id_common,
                this.common_state,
                this.creation_date,
                this.modify_date
        );
    }

    public void setCommon(Common commonNotification) {
        this.id_common = commonNotification.getId_common_state();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public Long getId_employee() {
        return id_employee;
    }

    public void setId_employee(Long id_employee) {
        this.id_employee = id_employee;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Date getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(Date begin_date) {
        this.begin_date = begin_date;
    }

    public Date getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(Date finish_date) {
        this.finish_date = finish_date;
    }

    public String getName_sale() {
        return name_sale;
    }

    public void setName_sale(String name_sale) {
        this.name_sale = name_sale;
    }

    public String getDescription_sale() {
        return description_sale;
    }

    public void setDescription_sale(String description_sale) {
        this.description_sale = description_sale;
    }

    public Long getTotal_value() {
        return total_value;
    }

    public void setTotal_value(Long total_value) {
        this.total_value = total_value;
    }

    public Long getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(Long discount_percent) {
        this.discount_percent = discount_percent;
    }

    public Long getId_state() {
        return id_state;
    }

    public void setId_state(Long id_state) {
        this.id_state = id_state;
    }

    public String getName_state() {
        return name_state;
    }

    public void setName_state(String name_state) {
        this.name_state = name_state;
    }

    public String getDescription_state() {
        return description_state;
    }

    public void setDescription_state(String description_state) {
        this.description_state = description_state;
    }

    public String getType_state() {
        return type_state;
    }

    public void setType_state(String type_state) {
        this.type_state = type_state;
    }

    public Long getId_common_state() {
        return id_common_state;
    }

    public void setId_common_state(Long id_common_state) {
        this.id_common_state = id_common_state;
    }

    public Integer getState_state() {
        return state_state;
    }

    public void setState_state(Integer state_state) {
        this.state_state = state_state;
    }

    public Date getCreation_date_state() {
        return creation_date_state;
    }

    public void setCreation_date_state(Date creation_date_state) {
        this.creation_date_state = creation_date_state;
    }

    public Date getModify_date_state() {
        return modify_date_state;
    }

    public void setModify_date_state(Date modify_date_state) {
        this.modify_date_state = modify_date_state;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Integer getCommon_state() {
        return common_state;
    }

    public void setCommon_state(Integer common_state) {
        this.common_state = common_state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
