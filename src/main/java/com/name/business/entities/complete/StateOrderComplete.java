package com.name.business.entities.complete;

import com.name.business.entities.Common;

import java.util.Date;

public class StateOrderComplete {

    private Long id_state;
    private String name_state;
    private String description;
    private String type_state;
    private Long id_common;
    private Integer common_state;
    private Date creation_date;
    private Date modify_date;

    public StateOrderComplete(Long id_state, String name_state, String description, String type_state, Common common) {
        this.id_state = id_state;
        this.name_state = name_state;
        this.description = description;
        this.type_state = type_state;
        this.id_common = common.getId_common_state();
        this.common_state = common.getState();
        this.creation_date = common.getCreation_date();
        this.modify_date = common.getModify_date();
    }

    public Common getCommon(){
        return new Common(
                this.id_common,
                this.common_state,
                this.creation_date,
                this.modify_date
        );
    }

    public void setCommon(Common commonNotification){
        this.id_common = commonNotification.getId_common_state();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public Long getId_state() {
        return id_state;
    }

    public void setId_state(Long id_state) {
        this.id_state = id_state;
    }

    public String getName_state() {
        return name_state;
    }

    public void setName_state(String name_state) {
        this.name_state = name_state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType_state() {
        return type_state;
    }

    public void setType_state(String type_state) {
        this.type_state = type_state;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public Integer getCommon_state() {
        return common_state;
    }

    public void setCommon_state(Integer common_state) {
        this.common_state = common_state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
