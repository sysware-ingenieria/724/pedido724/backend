package com.name.business.entities.complex;

import com.name.business.entities.complete.DetailOrderComplete;
import com.name.business.entities.complete.DetailSaleComplete;
import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.complete.SaleComplete;

import java.util.List;

public class OrderComplex {

    private OrdersComplete ordersComplete;
    private List<DetailOrderComplete> detailOrdersCompleteList;

    public OrderComplex(OrdersComplete ordersComplete, List<DetailOrderComplete> detailOrdersCompleteList) {
        this.ordersComplete = ordersComplete;
        this.detailOrdersCompleteList = detailOrdersCompleteList;
    }

    public OrdersComplete getOrdersComplete() {
        return ordersComplete;
    }

    public void setOrdersComplete(OrdersComplete ordersComplete) {
        this.ordersComplete = ordersComplete;
    }

    public List<DetailOrderComplete> getDetailOrdersCompleteList() {
        return detailOrdersCompleteList;
    }

    public void setDetailOrdersCompleteList(List<DetailOrderComplete> detailOrdersCompleteList) {
        this.detailOrdersCompleteList = detailOrdersCompleteList;
    }
}
