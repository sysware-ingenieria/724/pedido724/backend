package com.name.business.entities.complex;

import com.name.business.entities.Common;
import com.name.business.entities.complete.DetailSaleComplete;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complete.StateOrderComplete;

import java.util.Date;
import java.util.List;

public class SaleComplex {

    private SaleComplete saleComplete;
    private List<DetailSaleComplete> detailSaleCompleteList;

    public SaleComplex(SaleComplete saleComplete, List<DetailSaleComplete> detailSaleCompleteList) {
        this.saleComplete = saleComplete;
        this.detailSaleCompleteList = detailSaleCompleteList;
    }

    public SaleComplete getSaleComplete() {
        return saleComplete;
    }

    public void setSaleComplete(SaleComplete saleComplete) {
        this.saleComplete = saleComplete;
    }

    public List<DetailSaleComplete> getDetailSaleCompleteList() {
        return detailSaleCompleteList;
    }

    public void setDetailSaleCompleteList(List<DetailSaleComplete> detailSaleCompleteList) {
        this.detailSaleCompleteList = detailSaleCompleteList;
    }
}
