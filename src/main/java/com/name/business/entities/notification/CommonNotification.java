package com.name.business.entities.notification;

import java.util.Date;

public class CommonNotification {

    private Long id_common_Notif;
    private Integer state;
    private Date creation_date;
    private Date modify_date;

    public CommonNotification(Long id_common_Notif, Integer state, Date creation_date, Date modify_date) {
        this.id_common_Notif = id_common_Notif;
        this.state = state;
        this.creation_date = creation_date;
        this.modify_date = modify_date;
    }

    public Long getId_common_Notif() {
        return id_common_Notif;
    }

    public void setId_common_Notif(Long id_common_Notif) {
        this.id_common_Notif = id_common_Notif;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
