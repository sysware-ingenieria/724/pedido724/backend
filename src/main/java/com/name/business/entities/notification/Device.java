package com.name.business.entities.notification;

public class Device {
    private Long id_device;
    private Long device_number;
    private Long uuid;
    private Long device_type;
    private Long id_common;

    public Device(Long id_device, Long device_number, Long uuid, Long device_type, Long id_common) {
        this.id_device = id_device;
        this.device_number = device_number;
        this.uuid = uuid;
        this.device_type = device_type;
        this.id_common = id_common;
    }

    public Long getId_device() {
        return id_device;
    }

    public void setId_device(Long id_device) {
        this.id_device = id_device;
    }

    public Long getDevice_number() {
        return device_number;
    }

    public void setDevice_number(Long device_number) {
        this.device_number = device_number;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Long getDevice_type() {
        return device_type;
    }

    public void setDevice_type(Long device_type) {
        this.device_type = device_type;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }
}
