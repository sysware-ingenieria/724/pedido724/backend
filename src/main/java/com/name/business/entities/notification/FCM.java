package com.name.business.entities.notification;

import java.sql.Timestamp;

/**
 * Created by Niki on -/01/17.
 */
public class FCM {

    private Long id;
    private Long id_common_notification;
    private String title;
    private String description;
    private String data_info;
    private Integer state_fcm;
    private Timestamp sent_date;
    private String type_fcm;

    public FCM(Long id, Long id_common_notification, String title, String description, String data_info, Integer state_fcm, Timestamp sent_date, String type_fcm) {
        this.id = id;
        this.id_common_notification = id_common_notification;
        this.title = title;
        this.description = description;
        this.data_info = data_info;
        this.state_fcm = state_fcm;
        this.sent_date = sent_date;
        this.type_fcm = type_fcm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_common() {
        return id_common_notification;
    }

    public void setId_common(Long id_common) {
        this.id_common_notification = id_common;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData_info() {
        return data_info;
    }

    public void setData_info(String data_info) {
        this.data_info = data_info;
    }

    public Integer getState_fcm() {
        return state_fcm;
    }

    public void setState_fcm(Integer state_fcm) {
        this.state_fcm = state_fcm;
    }

    public Timestamp getSent_date() {
        return sent_date;
    }

    public void setSent_date(Timestamp sent_date) {
        this.sent_date = sent_date;
    }

    public String getType_fcm() {
        return type_fcm;
    }

    public void setType_fcm(String type_fcm) {
        this.type_fcm = type_fcm;
    }
}
