package com.name.business.entities.notification.complete;

import com.name.business.entities.Common;
import com.name.business.entities.notification.CommonNotification;

import java.util.Date;

public class DeviceComplete {
    private Long id_device;
    private Long device_number;
    private Long uuid;
    private String device_type;

    private Long id_common_notification;
    private Integer common_state;
    private Date creation_date;
    private Date modify_date;

    public DeviceComplete(Long id_device, Long device_number, Long uuid, String device_type, CommonNotification commonNotification) {
        this.id_device = id_device;
        this.device_number = device_number;
        this.uuid = uuid;
        this.device_type = device_type;

        this.id_common_notification = commonNotification.getId_common_Notif();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public CommonNotification getCommon(){
        return new CommonNotification(this.id_common_notification,
                this.common_state,
                this.creation_date,
                this.modify_date);
    }

    public void setCommon(CommonNotification commonNotification){
        this.id_common_notification = commonNotification.getId_common_Notif();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public Long getId_device() {
        return id_device;
    }

    public void setId_device(Long id_device) {
        this.id_device = id_device;
    }

    public Long getDevice_number() {
        return device_number;
    }

    public void setDevice_number(Long device_number) {
        this.device_number = device_number;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public Long getId_common_notification() {
        return id_common_notification;
    }

    public void setId_common_notification(Long id_common_notification) {
        this.id_common_notification = id_common_notification;
    }

    public Integer getCommon_state() {
        return common_state;
    }

    public void setCommon_state(Integer common_state) {
        this.common_state = common_state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
