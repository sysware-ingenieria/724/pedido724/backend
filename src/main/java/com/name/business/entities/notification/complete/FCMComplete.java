package com.name.business.entities.notification.complete;

import com.name.business.entities.notification.CommonNotification;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Niki on -/1/18.
 */
public class FCMComplete {

    private Long id;
    private String title;
    private String description;
    private String data_info;
    private String state_fcm;
    private Timestamp sent_date;
    private String type_fcm;
    private Long id_common_notification;
    private Integer common_state;
    private Date creation_date;
    private Date modify_date;

    public FCMComplete(Long id, String title, String description, String data_info, String state_fcm, String type_fcm, Timestamp sent_date, CommonNotification commonNotification) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.data_info = data_info;
        this.state_fcm = state_fcm;
        this.sent_date = sent_date;
        this.type_fcm = type_fcm;
        this.id_common_notification = commonNotification.getId_common_Notif();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
        this.common_state = commonNotification.getState();
    }

    public CommonNotification getCommon(){
        return new CommonNotification(this.id_common_notification,
                this.common_state,
                this.creation_date,
                this.modify_date);
    }

    public void setCommon(CommonNotification commonNotification){
        this.id_common_notification = commonNotification.getId_common_Notif();
        this.common_state = commonNotification.getState();
        this.creation_date = commonNotification.getCreation_date();
        this.modify_date = commonNotification.getModify_date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData_info() {
        return data_info;
    }

    public void setData_info(String data_info) {
        this.data_info = data_info;
    }

    public String getState_fcm() {
        return state_fcm;
    }

    public void setState_fcm(String state_fcm) {
        this.state_fcm = state_fcm;
    }

    public Timestamp getSent_date() {
        return sent_date;
    }

    public void setSent_date(Timestamp sent_date) {
        this.sent_date = sent_date;
    }

    public String getType_fcm() {
        return type_fcm;
    }

    public void setType_fcm(String type_fcm) {
        this.type_fcm = type_fcm;
    }

    public Long getId_common_notification() {
        return id_common_notification;
    }

    public void setId_common_notification(Long id_common_notification) {
        this.id_common_notification = id_common_notification;
    }

    public Integer getCommon_state() {
        return common_state;
    }

    public void setCommon_state(Integer common_state) {
        this.common_state = common_state;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
