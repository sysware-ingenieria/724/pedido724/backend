package com.name.business.mappers.complete;

import com.name.business.entities.Common;
import com.name.business.entities.complete.DetailOrderComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailOrderCompleteMapper implements ResultSetMapper<DetailOrderComplete> {

    @Override
    public DetailOrderComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailOrderComplete(
                resultSet.getLong("ID_DETAIL_ORDER"),
                resultSet.getLong("ID_ORDER"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getInt("QUANTITY"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("VALUE_PRODUCT"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getInt("COMMON_STATE"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                )
        );
    }
}
