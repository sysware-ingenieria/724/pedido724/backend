package com.name.business.mappers.complete;

import com.name.business.entities.Common;
import com.name.business.entities.complete.DetailSaleComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailSaleCompleteMapper implements ResultSetMapper<DetailSaleComplete> {

    @Override
    public DetailSaleComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailSaleComplete(
                resultSet.getLong("ID_DETAIL_SALE"),
                resultSet.getLong("ID_SALE"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getLong("ORIGINAL_VALUE"),
                resultSet.getLong("SALE_VALUE"),
                resultSet.getInt("QUANTITY"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getInt("COMMON_STATE"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                )
        );
    }
}
