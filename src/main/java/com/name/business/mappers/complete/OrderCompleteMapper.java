package com.name.business.mappers.complete;

import com.name.business.entities.Common;
import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.complete.StateOrderComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderCompleteMapper implements ResultSetMapper<OrdersComplete> {

    @Override
    public OrdersComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new OrdersComplete(
                resultSet.getLong("ID_ORDER"),
                resultSet.getLong("ID_THIRD_CLIENT"),
                resultSet.getLong("ORDER_NUMBER"),
                resultSet.getLong("ID_SALE"),
                new StateOrderComplete(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getString("NAME_STATE"),
                        resultSet.getString("DESCRIPTION_STATE"),
                        resultSet.getString("TYPE_STATE"),
                        new Common(
                                resultSet.getLong("ID_COMMON_STATE"),
                                resultSet.getInt("COMMON_STATE_STATE"),
                                resultSet.getDate("CREATION_DATE_STATE"),
                                resultSet.getDate("MODIFY_DATE_STATE")
                        )
                ),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getInt("COMMON_STATE"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                )
        );
    }
}
