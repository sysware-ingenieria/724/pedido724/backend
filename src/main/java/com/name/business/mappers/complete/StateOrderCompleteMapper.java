package com.name.business.mappers.complete;

import com.name.business.entities.Common;
import com.name.business.entities.complete.StateOrderComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StateOrderCompleteMapper implements ResultSetMapper<StateOrderComplete> {

    @Override
    public StateOrderComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new StateOrderComplete(
                resultSet.getLong("ID_STATE"),
                resultSet.getString("NAME_STATE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("TYPE_STATE"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getInt("COMMON_STATE"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                )
        );
    }
}
