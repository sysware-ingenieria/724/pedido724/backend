package com.name.business.mappers.complex;

import com.name.business.entities.Common;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.entities.complex.SaleComplex;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SaleComplexMapper implements ResultSetMapper<SaleComplex> {

    @Override
    public SaleComplex map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new SaleComplex(
                new SaleComplete(
                        resultSet.getLong("ID_SALE"),
                        resultSet.getLong("ID_EMPLOYEE"),
                        resultSet.getLong("ID_THIRD"),
                        resultSet.getDate("BEGIN_DATE"),
                        resultSet.getDate("FINISH_DATE"),
                        resultSet.getString("NAME_SALE"),
                        resultSet.getString("DESCRIPTION"),
                        resultSet.getLong("TOTAL_VALUE"),
                        resultSet.getLong("DISCOUNT_PERCENT"),
                        new StateOrderComplete(
                                resultSet.getLong("ID_STATE"),
                                resultSet.getString("NAME_STATE"),
                                resultSet.getString("DESCRIPTION_STATE"),
                                resultSet.getString("TYPE_STATE"),
                                new Common(
                                        resultSet.getLong("ID_COMMON_STATE"),
                                        resultSet.getInt("COMMON_STATE_STATE"),
                                        resultSet.getDate("CREATION_DATE_STATE"),
                                        resultSet.getDate("MODIFY_DATE_STATE")
                                )
                        ),
                        new Common(
                                resultSet.getLong("ID_COMMON"),
                                resultSet.getInt("COMMON_STATE"),
                                resultSet.getDate("CREATION_DATE"),
                                resultSet.getDate("MODIFY_DATE")
                        )
                ),
                null
        );
    }
}
