package com.name.business.mappers.notification;

import com.name.business.entities.Common;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommonNotificationMapper implements ResultSetMapper<Common> {

    @Override
    public Common map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Common(
                resultSet.getLong("ID_COMMON_NOTIF"),
                resultSet.getInt("STATE_NOTIF"),
                resultSet.getDate("CREATION_DATE"),
                resultSet.getDate("MODIFY_DATE")
        );
    }
}
