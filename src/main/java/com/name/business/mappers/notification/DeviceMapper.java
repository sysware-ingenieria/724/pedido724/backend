package com.name.business.mappers.notification;

import com.name.business.entities.Common;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.complete.DeviceComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DeviceMapper implements ResultSetMapper<DeviceComplete> {

    @Override
    public DeviceComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DeviceComplete(
                resultSet.getLong("ID_DEVICE"),
                resultSet.getLong("DEVICE_NUMBER"),
                resultSet.getLong("UUID"),
                resultSet.getString("DEVICE_TYPE"),
                new CommonNotification(
                        resultSet.getLong("ID_COMMON_NOTIF"),
                        resultSet.getInt("STATE_NOTIF"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                ));
    }
}
