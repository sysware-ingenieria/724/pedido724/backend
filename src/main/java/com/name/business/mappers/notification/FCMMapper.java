package com.name.business.mappers.notification;

import com.name.business.entities.Common;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.FCM;
import com.name.business.entities.notification.complete.FCMComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FCMMapper implements ResultSetMapper<FCMComplete> {

    @Override
    public FCMComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new FCMComplete(
                resultSet.getLong("ID_FCM"),
                resultSet.getString("TITLE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("DATAINFO"),
                resultSet.getString("STATE_FCM"),
                resultSet.getString("TYPE_FCM"),
                resultSet.getTimestamp("SEND_DATE"),
                new CommonNotification(
                        resultSet.getLong("ID_COMMON_NOTIF"),
                        resultSet.getInt("STATE_NOTIF"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                )
        );
    }
}
