package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailOrderDTO {
    private Long id_order;
    private Long id_product_third;
    private Integer quantity;
    private Long id_third;
    private Long value_product;
    private CommonDTO commonDTO;

    @JsonCreator
    public DetailOrderDTO(
            @JsonProperty("id_order") Long id_order,
            @JsonProperty("id_product_third") Long id_product_third,
            @JsonProperty("quantity") Integer quantity,
            @JsonProperty("id_third") Long id_third,
            @JsonProperty("value_product") Long value_product,
            @JsonProperty("commonDTO") CommonDTO commonDTO)
    {
        this.id_order = id_order;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.id_third = id_third;
        this.value_product = value_product;
        this.commonDTO = commonDTO;
    }

    public Long getId_order() {
        return id_order;
    }

    public void setId_order(Long id_order) {
        this.id_order = id_order;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getValue_product() {
        return value_product;
    }

    public void setValue_product(Long value_product) {
        this.value_product = value_product;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }
}
