package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.representations.notification.CommonNotificationDTO;

public class DetailSaleDTO {
    private Long id_sale;
    private Long id_third;
    private Long id_product_third;
    private Long original_value;
    private Long sale_value;
    private Integer quantity;
    private CommonDTO commonDTO;

    @JsonCreator
    public DetailSaleDTO(
            @JsonProperty("id_sale") Long id_sale,
            @JsonProperty("id_third") Long id_third,
            @JsonProperty("id_product_third") Long id_product_third,
            @JsonProperty("original_value") Long original_value,
            @JsonProperty("sale_value") Long sale_value,
            @JsonProperty("quantity") Integer quantity,
            @JsonProperty("common") CommonDTO commonDTO)
    {
        this.id_sale = id_sale;
        this.id_third = id_third;
        this.id_product_third = id_product_third;
        this.original_value = original_value;
        this.sale_value = sale_value;
        this.quantity = quantity;
        this.commonDTO = commonDTO;
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Long getOriginal_value() {
        return original_value;
    }

    public void setOriginal_value(Long original_value) {
        this.original_value = original_value;
    }

    public Long getSale_value() {
        return sale_value;
    }

    public void setSale_value(Long sale_value) {
        this.sale_value = sale_value;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }
}
