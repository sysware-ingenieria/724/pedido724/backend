package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrdersDTO {

    private Long id_third_client;
    private Long order_number;
    private Long id_sale;
    private StateOrderDTO stateOrderDTO;
    private CommonDTO commonDTO;

    @JsonCreator
    public OrdersDTO(
            @JsonProperty("id_third_client") Long id_third_client,
            @JsonProperty("order_number") Long order_number,
            @JsonProperty("id_sale") Long id_sale,
            @JsonProperty("stateOrder") StateOrderDTO stateOrderDTO,
            @JsonProperty("common") CommonDTO commonDTO)
    {
        this.id_third_client = id_third_client;
        this.order_number = order_number;
        this.id_sale = id_sale;
        this.stateOrderDTO = stateOrderDTO;
        this.commonDTO = commonDTO;
    }

    public Long getId_third_client() {
        return id_third_client;
    }

    public void setId_third_client(Long id_third_client) {
        this.id_third_client = id_third_client;
    }

    public Long getOrder_number() {
        return order_number;
    }

    public void setOrder_number(Long order_number) {
        this.order_number = order_number;
    }

    public Long getId_sale() {
        return id_sale;
    }

    public void setId_sale(Long id_sale) {
        this.id_sale = id_sale;
    }

    public StateOrderDTO getStateOrderDTO() {
        return stateOrderDTO;
    }

    public void setStateOrderDTO(StateOrderDTO stateOrderDTO) {
        this.stateOrderDTO = stateOrderDTO;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }
}
