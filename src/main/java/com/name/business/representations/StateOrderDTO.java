package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StateOrderDTO {

    private String name_state;
    private String description;
    private String type_state;
    private CommonDTO commonDTO;

    @JsonCreator
    public StateOrderDTO(
            @JsonProperty("name_state") String name_state,
            @JsonProperty("description") String description,
            @JsonProperty("type_state") String type_state,
            @JsonProperty("common") CommonDTO commonDTO) {

        this.name_state = name_state;
        this.description = description;
        this.type_state = type_state;
        this.commonDTO = commonDTO;
    }

    public String getName_state() {
        return name_state;
    }

    public void setName_state(String name_state) {
        this.name_state = name_state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType_state() {
        return type_state;
    }

    public void setType_state(String type_state) {
        this.type_state = type_state;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }
}
