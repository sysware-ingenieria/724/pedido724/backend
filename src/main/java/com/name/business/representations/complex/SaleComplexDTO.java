package com.name.business.representations.complex;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.representations.CommonDTO;
import com.name.business.representations.DetailSaleDTO;
import com.name.business.representations.SaleDTO;
import com.name.business.representations.StateOrderDTO;

import java.util.Date;
import java.util.List;

public class SaleComplexDTO {

    private Long id_employee;
    private Long id_third;
    private Date begin_date;
    private Date finish_date;
    private String name_sale;
    private String description;
    private Long total_value;
    private Long discount_percent;
    private StateOrderDTO stateOrderDTO;
    private CommonDTO commonDTO;
    private List<DetailSaleDTO> detailSaleDTO;

    @JsonCreator
    public SaleComplexDTO(
            @JsonProperty("id_employee") Long id_employee,
            @JsonProperty("id_third") Long id_third,
            @JsonProperty("begin_date") Date begin_date,
            @JsonProperty("finish_date") Date finish_date,
            @JsonProperty("name_sale") String name_sale,
            @JsonProperty("description") String description,
            @JsonProperty("total_value") Long total_value,
            @JsonProperty("discount_percent") Long discount_percent,
            @JsonProperty("stateOrder") StateOrderDTO stateOrderDTO,
            @JsonProperty("common") CommonDTO commonDTO,
            @JsonProperty("detailSale") List<DetailSaleDTO> detailSaleDTO) {
        this.id_employee = id_employee;
        this.id_third = id_third;
        this.begin_date = begin_date;
        this.finish_date = finish_date;
        this.name_sale = name_sale;
        this.description = description;
        this.total_value = total_value;
        this.discount_percent = discount_percent;
        this.stateOrderDTO = stateOrderDTO;
        this.commonDTO = commonDTO;
        this.detailSaleDTO = detailSaleDTO;
    }

    public SaleDTO getSaleDTO() {
        return new SaleDTO(
                id_employee,
                id_third,
                begin_date,
                finish_date,
                name_sale,
                description,
                total_value,
                discount_percent,
                stateOrderDTO,
                commonDTO
                );
    }

    public void setSaleDTO(SaleDTO saleDTO){
        this.id_employee = saleDTO.getId_employee();
        this.id_third = saleDTO.getId_third();
        this.begin_date = saleDTO.getBegin_date();
        this.finish_date = saleDTO.getFinish_date();
        this.name_sale = saleDTO.getName_sale();
        this.description = saleDTO.getDescription();
        this.total_value = saleDTO.getTotal_value();
        this.discount_percent = saleDTO.getDiscount_percent();
        this.stateOrderDTO = saleDTO.getStateOrderDTO();
        this.commonDTO = saleDTO.getCommonDTO();
    }

    public List<DetailSaleDTO> getDetailSaleDTO() {
        return detailSaleDTO;
    }

    public void setDetailSaleDTO(List<DetailSaleDTO> detailSaleDTO) {
        this.detailSaleDTO = detailSaleDTO;
    }

    public Long getId_employee() {
        return id_employee;
    }

    public void setId_employee(Long id_employee) {
        this.id_employee = id_employee;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Date getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(Date begin_date) {
        this.begin_date = begin_date;
    }

    public Date getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(Date finish_date) {
        this.finish_date = finish_date;
    }

    public String getName_sale() {
        return name_sale;
    }

    public void setName_sale(String name_sale) {
        this.name_sale = name_sale;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTotal_value() {
        return total_value;
    }

    public void setTotal_value(Long total_value) {
        this.total_value = total_value;
    }

    public Long getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(Long discount_percent) {
        this.discount_percent = discount_percent;
    }

    public StateOrderDTO getStateOrderDTO() {
        return stateOrderDTO;
    }

    public void setStateOrderDTO(StateOrderDTO stateOrderDTO) {
        this.stateOrderDTO = stateOrderDTO;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }
}
