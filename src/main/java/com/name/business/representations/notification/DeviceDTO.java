package com.name.business.representations.notification;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeviceDTO {
    private Long device_number;
    private Long uuid;
    private String device_type;
    private CommonNotificationDTO commonDTO;

    @JsonCreator
    public DeviceDTO(
            @JsonProperty("device_number") Long device_number,
            @JsonProperty("uuid") Long uuid,
            @JsonProperty("device_type") String device_type,
            @JsonProperty("common") CommonNotificationDTO commonDTO)
    {
        this.device_number = device_number;
        this.uuid = uuid;
        this.device_type = device_type;
        this.commonDTO = commonDTO;
    }

    public Long getDevice_number() {
        return device_number;
    }

    public void setDevice_number(Long device_number) {
        this.device_number = device_number;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public CommonNotificationDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonNotificationDTO commonDTO) {
        this.commonDTO = commonDTO;
    }
}
