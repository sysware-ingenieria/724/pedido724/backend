package com.name.business.representations.notification;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

/**
 * Created by luis on 8/04/17.
 */
public class FCMDTO {


    private String title;
    private String description;
    private String data_info;
    private String state_fcm;
    private String type;
    private Timestamp date_sent;
    private CommonNotificationDTO commonNotificationDTO;

    @JsonCreator
    public FCMDTO(
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("data_info") String data_info,
            @JsonProperty("state_fcm") String state_fcm,
            @JsonProperty("type") String type,
            @JsonProperty("date_sent") Timestamp date_sent,
            @JsonProperty("common") CommonNotificationDTO commonNotificationDTO)
    {

        this.title = title;
        this.description = description;
        this.data_info = data_info;
        this.state_fcm = state_fcm;
        this.date_sent = date_sent;
        this.type=type;
        this.commonNotificationDTO = commonNotificationDTO;
    }

    public CommonNotificationDTO getCommonNotificationDTO() {
        return commonNotificationDTO;
    }

    public void setCommonNotificationDTO(CommonNotificationDTO commonNotificationDTO) {
        this.commonNotificationDTO = commonNotificationDTO;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData_info() {
        return data_info;
    }

    public void setData_info(String data_info) {
        this.data_info = data_info;
    }

    public String getState_fcm() {
        return state_fcm;
    }

    public void setState_fcm(String state_fcm) {
        this.state_fcm = state_fcm;
    }

    public Timestamp getDate_sent() {
        return date_sent;
    }

    public void setDate_sent(Timestamp date_sent) {
        this.date_sent = date_sent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
