package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DetailOrderBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.complete.DetailOrderComplete;
import com.name.business.representations.DetailOrderDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/detailOrders")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailOrderResource {

    private DetailOrderBusiness detailOrderBusiness;

    public DetailOrderResource(DetailOrderBusiness detailOrderBusiness) {
        this.detailOrderBusiness = detailOrderBusiness;
    }

    @GET
    @Timed
    public Response getDetailOrdersResourceList(
            @QueryParam("id_detail_order") Long id_detail_order,
            @QueryParam("id_order") Long id_order,
            @QueryParam("id_product_third") Long id_product_third,
            @QueryParam("quantity") Integer quantity,
            @QueryParam("id_third") Long id_third,
            @QueryParam("value_product") Long value_product,
            @QueryParam("id_common") Long id_common,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<DetailOrderComplete>> getDetailOrders = detailOrderBusiness.get(
                new DetailOrderComplete(
                        id_detail_order,
                        id_order,
                        id_product_third,
                        quantity,
                        id_third,
                        value_product,
                        new Common(
                                id_common,
                                common_state,
                                creation_date,
                                modify_date
                        )
                ));

        if (getDetailOrders.isRight()) {
            System.out.println(getDetailOrders.right().value().size());
            response = Response.status(Response.Status.OK).entity(getDetailOrders.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getDetailOrders);
        }
        return response;
    }

    /**
     * @param detailOrderDTO
     * @return status
     */
    @POST
    @Timed
    public Response postDetailOrdersResource(DetailOrderDTO detailOrderDTO) {
        Response response;

        Either<IException, Long> mailEither = detailOrderBusiness.create(detailOrderDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateDetailOrdersResource(@PathParam("id") Long id_DetailOrders, DetailOrderDTO DetailOrdersDTO) {
        Response response;
        Either<IException, Long> allViewOffertsEither = detailOrderBusiness.update(id_DetailOrders, DetailOrdersDTO);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteDetailOrdersResource(@PathParam("id") Long id_DetailOrders) {
        Response response;
        Either<IException, Long> allViewOffertsEither = detailOrderBusiness.delete(id_DetailOrders);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteDetailOrdersResource(@PathParam("id") Long id_DetailOrders) {
        Response response;
        Either<IException, Long> allViewOffertsEither = detailOrderBusiness.permanentDelete(id_DetailOrders);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
