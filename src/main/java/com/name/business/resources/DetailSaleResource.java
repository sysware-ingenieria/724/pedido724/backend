package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DetailSaleBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.complete.DetailSaleComplete;
import com.name.business.representations.DetailSaleDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/detailSale")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailSaleResource {

    private DetailSaleBusiness detailSaleBusiness;

    public DetailSaleResource(DetailSaleBusiness detailSaleBusiness) {
        this.detailSaleBusiness = detailSaleBusiness;
    }

    @GET
    @Timed
    public Response getDetailSaleResourceList(
            @QueryParam("id_detail_sale") Long id_detail_sale,
            @QueryParam("id_sale") Long id_sale,
            @QueryParam("id_third") Long id_third,
            @QueryParam("id_product_third") Long id_product_third,
            @QueryParam("original_value") Long original_value,
            @QueryParam("sale_value") Long sale_value,
            @QueryParam("quantity") Integer quantity,
            @QueryParam("id_common") Long id_common,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<DetailSaleComplete>> getDetailSale = detailSaleBusiness.get(
                new DetailSaleComplete(
                        id_detail_sale,
                        id_sale,
                        id_third,
                        id_product_third,
                        original_value,
                        sale_value,
                        quantity,
                        new Common(
                                id_common,
                                common_state,
                                creation_date,
                                modify_date
                        )
                ));

        if (getDetailSale.isRight()) {
            System.out.println(getDetailSale.right().value().size());
            response = Response.status(Response.Status.OK).entity(getDetailSale.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getDetailSale);
        }
        return response;
    }

    /**
     * @param detailSaleDTO
     * @return status
     */
    @POST
    @Timed
    public Response postDetailSaleResource(DetailSaleDTO detailSaleDTO) {
        Response response;

        Either<IException, Long> mailEither = detailSaleBusiness.create(detailSaleDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateDetailSaleResource(@PathParam("id") Long id_DetailSale, DetailSaleDTO DetailSaleDTO) {
        Response response;
        Either<IException, Long> allViewOffertsEither = detailSaleBusiness.update(id_DetailSale, DetailSaleDTO);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteDetailSaleResource(@PathParam("id") Long id_DetailSale) {
        Response response;
        Either<IException, Long> allViewOffertsEither = detailSaleBusiness.delete(id_DetailSale);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteDetailSaleResource(@PathParam("id") Long id_DetailSale) {
        Response response;
        Either<IException, Long> allViewOffertsEither = detailSaleBusiness.permanentDelete(id_DetailSale);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
