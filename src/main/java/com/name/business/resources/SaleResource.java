package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.SaleBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.complete.SaleComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.representations.SaleDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/sales")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SaleResource {

    private SaleBusiness saleBusiness;

    public SaleResource(SaleBusiness saleBusiness) {
        this.saleBusiness = saleBusiness;
    }

    @GET
    @Timed
    public Response getSaleResourceList(
            @QueryParam("id_sale") Long id_sale,
            @QueryParam("id_employee") Long id_employee,
            @QueryParam("id_third") Long id_third,
            @QueryParam("begin_date") Date begin_date,
            @QueryParam("finish_date") Date finish_date,
            @QueryParam("name_sale") String name_sale,
            @QueryParam("description_sale") String description_sale,
            @QueryParam("total_value") Long total_value,
            @QueryParam("discount_percent") Long discount_percent,
            @QueryParam("id_state") Long id_state,
            @QueryParam("name_state") String name_state,
            @QueryParam("description_state") String description_state,
            @QueryParam("type_state") String type_state,
            @QueryParam("id_common_state") Long id_common_state,
            @QueryParam("state_state") Integer state_state,
            @QueryParam("creation_date_state") Date creation_date_state,
            @QueryParam("modify_date_state") Date modify_date_state,
            @QueryParam("id_common") Long id_common,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<SaleComplete>> getSale = saleBusiness.get(
                new SaleComplete(
                        id_state,
                        id_employee,
                        id_third,
                        begin_date,
                        finish_date,
                        name_sale,
                        description_sale,
                        total_value,
                        discount_percent,
                        new StateOrderComplete(
                                id_state,
                                name_state,
                                description_state,
                                type_state,
                                new Common(
                                        id_common_state,
                                        state_state,
                                        creation_date_state,
                                        modify_date_state
                                )),
                        new Common(
                                id_common,
                                common_state,
                                creation_date,
                                modify_date
                        )
                ));

        if (getSale.isRight()) {
            System.out.println(getSale.right().value().size());
            response = Response.status(Response.Status.OK).entity(getSale.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getSale);
        }
        return response;
    }

    /**
     * @param saleDTO
     * @return status
     */
    @POST
    @Timed
    public Response postSaleResource(SaleDTO saleDTO) {
        Response response;

        Either<IException, Long> mailEither = saleBusiness.create(saleDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateSaleResource(@PathParam("id") Long id_Sale, SaleDTO SaleDTO) {
        Response response;
        Either<IException, Long> allViewOffertsEither = saleBusiness.update(id_Sale, SaleDTO);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteSaleResource(@PathParam("id") Long id_Sale) {
        Response response;
        Either<IException, Long> allViewOffertsEither = saleBusiness.delete(id_Sale);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteSaleResource(@PathParam("id") Long id_Sale) {
        Response response;
        Either<IException, Long> allViewOffertsEither = saleBusiness.permanentDelete(id_Sale);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
