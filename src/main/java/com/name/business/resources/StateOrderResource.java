package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.StateOrderBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.representations.StateOrderDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/stateOrders")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StateOrderResource {

    private StateOrderBusiness stateOrderBusiness;

    public StateOrderResource(StateOrderBusiness stateOrderBusiness) {
        this.stateOrderBusiness = stateOrderBusiness;
    }

    @GET
    @Timed
    public Response getStateOrderResourceList(
            @QueryParam("id_state") Long id_state,
            @QueryParam("name_state") String name_state,
            @QueryParam("description") String description,
            @QueryParam("type_state") String type_state,
            @QueryParam("id_common") Long id_common,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<StateOrderComplete>> getStateOrder = stateOrderBusiness.get(
                new StateOrderComplete(
                        id_state,
                        name_state,
                        description,
                        type_state,
                        new Common(id_common, common_state,
                                creation_date, modify_date)
                ));

        if (getStateOrder.isRight()) {
            System.out.println(getStateOrder.right().value().size());
            response = Response.status(Response.Status.OK).entity(getStateOrder.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getStateOrder);
        }
        return response;
    }

    /**
     * @param stateOrderDTO
     * @return status
     */
    @POST
    @Timed
    public Response postStateOrderResource(StateOrderDTO stateOrderDTO) {
        Response response;

        Either<IException, Long> mailEither = stateOrderBusiness.create(stateOrderDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateStateOrderResource(@PathParam("id") Long id_StateOrder, StateOrderDTO StateOrderDTO) {
        Response response;
        Either<IException, Long> allViewOffertsEither = stateOrderBusiness.update(id_StateOrder, StateOrderDTO);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteStateOrderResource(@PathParam("id") Long id_StateOrder) {
        Response response;
        Either<IException, Long> allViewOffertsEither = stateOrderBusiness.delete(id_StateOrder);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteStateOrderResource(@PathParam("id") Long id_StateOrder) {
        Response response;
        Either<IException, Long> allViewOffertsEither = stateOrderBusiness.permanentDelete(id_StateOrder);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
