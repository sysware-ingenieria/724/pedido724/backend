package com.name.business.resources.complex;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.complex.OrdersComplexBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.complete.OrdersComplete;
import com.name.business.entities.complete.StateOrderComplete;
import com.name.business.entities.complex.OrderComplex;
import com.name.business.representations.complex.OrdersComplexDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/extendedOrders")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OrderComplexResource {

    private OrdersComplexBusiness orderComplexBusiness;

    public OrderComplexResource(OrdersComplexBusiness orderComplexBusiness) {
        this.orderComplexBusiness = orderComplexBusiness;
    }

    @GET
    @Timed
    public Response getOrderComplexComplexResourceList(
            @QueryParam("id_order") Long id_order,
            @QueryParam("id_third_client") Long id_third_client,
            @QueryParam("order_number") Long order_number,
            @QueryParam("id_sale") Long id_sale,
            @QueryParam("id_state") Long id_state,
            @QueryParam("name_state") String name_state,
            @QueryParam("description_state") String description_state,
            @QueryParam("type_state") String type_state,
            @QueryParam("id_common_state") Long id_common_state,
            @QueryParam("state_state") Integer state_state,
            @QueryParam("creation_date_state") Date creation_date_state,
            @QueryParam("modify_date_state") Date modify_date_state,
            @QueryParam("id_common") Long id_common,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<OrderComplex>> getOrderComplex = orderComplexBusiness.get(
                new OrdersComplete(
                        id_state,
                        id_third_client,
                        order_number,
                        id_sale,
                        new StateOrderComplete(
                                id_state,
                                name_state,
                                description_state,
                                type_state,
                                new Common(
                                        id_common_state,
                                        state_state,
                                        creation_date_state,
                                        modify_date_state
                                )),
                        new Common(
                                id_common,
                                common_state,
                                creation_date,
                                modify_date
                        )
                ));

        if (getOrderComplex.isRight()) {
            System.out.println(getOrderComplex.right().value().size());
            response = Response.status(Response.Status.OK).entity(getOrderComplex.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getOrderComplex);
        }
        return response;
    }

    /**
     * @param orderComplexDTO
     * @return status
     */
    @POST
    @Timed
    public Response postOrderComplexResource(OrdersComplexDTO orderComplexDTO) {
        Response response;

        Either<IException, Long> mailEither = orderComplexBusiness.create(orderComplexDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteOrderComplexResource(@PathParam("id") Long id_OrderComplex) {
        Response response;
        Either<IException, Long> allViewOffertsEither = orderComplexBusiness.delete(id_OrderComplex);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteOrderComplexResource(@PathParam("id") Long id_OrderComplex) {
        Response response;
        Either<IException, Long> allViewOffertsEither = orderComplexBusiness.permanentDelete(id_OrderComplex);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
