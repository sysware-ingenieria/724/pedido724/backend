package com.name.business.resources.notification;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.notification.DeviceBusiness;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.complete.DeviceComplete;
import com.name.business.representations.notification.DeviceDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/devices")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeviceResource {

    private DeviceBusiness deviceBusiness;

    public DeviceResource(DeviceBusiness deviceBusiness) {
        this.deviceBusiness = deviceBusiness;
    }

    @GET
    @Timed
    public Response getDeviceResourceList(
            @QueryParam("id_device") Long id_device,
            @QueryParam("device_number") Long device_number,
            @QueryParam("uuid") Long uuid,
            @QueryParam("device_type") String device_type,
            @QueryParam("id_common_notification") Long id_common_notification,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<DeviceComplete>> getDevice = deviceBusiness.get(
                new DeviceComplete(
                        id_device,
                        device_number,
                        uuid,
                        device_type,
                        new CommonNotification(
                                id_common_notification,
                                common_state,
                                creation_date,
                                modify_date
                        )
                ));

        if (getDevice.isRight()) {
            System.out.println(getDevice.right().value().size());
            response = Response.status(Response.Status.OK).entity(getDevice.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getDevice);
        }
        return response;
    }

    /**
     * @param deviceDTO
     * @return status
     */
    @POST
    @Timed
    public Response postDeviceResource(DeviceDTO deviceDTO) {
        Response response;

        Either<IException, Long> mailEither = deviceBusiness.create(deviceDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateDeviceResource(@PathParam("id") Long id_Device, DeviceDTO DeviceDTO) {
        Response response;
        Either<IException, Long> allViewOffertsEither = deviceBusiness.update(id_Device, DeviceDTO);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteDeviceResource(@PathParam("id") Long id_Device) {
        Response response;
        Either<IException, Long> allViewOffertsEither = deviceBusiness.delete(id_Device);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteDeviceResource(@PathParam("id") Long id_Device) {
        Response response;
        Either<IException, Long> allViewOffertsEither = deviceBusiness.permanentDelete(id_Device);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
