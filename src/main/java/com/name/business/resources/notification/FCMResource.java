package com.name.business.resources.notification;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.notification.FCMBusiness;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.complete.FCMComplete;
import com.name.business.representations.notification.FCMDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Path("/fcms")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FCMResource {

    private FCMBusiness fcmBusiness;

    public FCMResource(FCMBusiness fcmBusiness) {
        this.fcmBusiness = fcmBusiness;
    }

    @GET
    @Timed
    public Response getFCMResourceList(
            @QueryParam("id") Long id,
            @QueryParam("title") String title,
            @QueryParam("description") String description,
            @QueryParam("data_info") String data_info,
            @QueryParam("state_fcm") String state_fcm,
            @QueryParam("sent_date") Date sent_date,
            @QueryParam("type_fcm") String type_fcm,
            @QueryParam("id_common_notification") Long id_common_notification,
            @QueryParam("common_state") Integer common_state,
            @QueryParam("creation_date") Date creation_date,
            @QueryParam("modify_date") Date modify_date
    ) {
        Response response;

        Either<IException, List<FCMComplete>> getFCM = fcmBusiness.get(
                new FCMComplete(
                        id,
                        title,
                        description,
                        data_info,
                        state_fcm,
                        type_fcm,
                        new Timestamp(sent_date.getTime()),
                        new CommonNotification(
                                id_common_notification,
                                common_state,
                                creation_date,
                                modify_date
                        )
                ));

        if (getFCM.isRight()) {
            System.out.println(getFCM.right().value().size());
            response = Response.status(Response.Status.OK).entity(getFCM.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getFCM);
        }
        return response;
    }

    /**
     * @param fcmDTO
     * @return status
     */
    @POST
    @Timed
    public Response postFCMResource(FCMDTO fcmDTO) {
        Response response;

        Either<IException, Long> mailEither = fcmBusiness.create(fcmDTO);
        if (mailEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    /*
        @POST
        @Timed
        public Response sendFCM(@QueryParam("to") String to, @QueryParam("idFCM") Long idFCM) {
            Response response;

            Either<IException, List<FCMComplete>> mailEither = fcmBusiness.get(
                    new FCMComplete(
                            idFCM,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            new CommonNotification(
                                    null,
                                    null,
                                    null,
                                    null
                            )
                    ));

            if (mailEither.isRight()) {
                response = Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
            } else {
                response = ExceptionResponse.createErrorResponse(mailEither);
            }
            return response;
        }
    */
    @Path("/{id}")
    @PUT
    @Timed
    public Response updateFCMResource(@PathParam("id") Long id_FCM, FCMDTO FCMDTO) {
        Response response;
        Either<IException, Long> allViewOffertsEither = fcmBusiness.update(id_FCM, FCMDTO);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteFCMResource(@PathParam("id") Long id_FCM) {
        Response response;
        Either<IException, Long> allViewOffertsEither = fcmBusiness.delete(id_FCM);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/pd/{id}")
    @DELETE
    @Timed
    public Response permanentDeleteFCMResource(@PathParam("id") Long id_FCM) {
        Response response;
        Either<IException, Long> allViewOffertsEither = fcmBusiness.permanentDelete(id_FCM);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
