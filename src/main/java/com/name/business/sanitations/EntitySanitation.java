package com.name.business.sanitations;

import com.name.business.entities.Common;
import com.name.business.entities.complete.*;
import com.name.business.entities.notification.CommonNotification;
import com.name.business.entities.notification.complete.DeviceComplete;
import com.name.business.entities.notification.complete.FCMComplete;

import java.sql.Timestamp;

import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class EntitySanitation {

    public static Common commonStateSanitation(Common commonState) {
        Common commonStateSanitation = null;
        commonStateSanitation = new Common(
                formatoLongSql(commonState.getId_common_state()),
                formatoIntegerSql(commonState.getState()),
                formatoDateSql(commonState.getCreation_date()),
                formatoDateSql(commonState.getModify_date())
        );
        return commonStateSanitation;
    }

    public static CommonNotification commonNotificationSanitation(CommonNotification commonState) {
        CommonNotification commonStateSanitation = null;
        commonStateSanitation = new CommonNotification(
                formatoLongSql(commonState.getId_common_Notif()),
                formatoIntegerSql(commonState.getState()),
                formatoDateSql(commonState.getCreation_date()),
                formatoDateSql(commonState.getModify_date())
        );

        return commonStateSanitation;
    }

    public static FCMComplete fcmSanitation(FCMComplete fcmComplete) {
        FCMComplete fcmCompleteSanitation = null;
        Timestamp sentDate = null;
        if(fcmComplete.getSent_date() != null){
            sentDate = new Timestamp(formatoDateSql(fcmComplete.getSent_date()).getTime());
        }
        fcmCompleteSanitation = new FCMComplete(
                formatoLongSql(fcmComplete.getId()),
                formatoStringSql(fcmComplete.getTitle()),
                formatoStringSql(fcmComplete.getDescription()),
                formatoStringSql(fcmComplete.getData_info()),
                formatoStringSql(fcmComplete.getState_fcm()),
                formatoStringSql(fcmComplete.getType_fcm()),
                sentDate,
                commonNotificationSanitation(fcmComplete.getCommon())
        );

        return fcmCompleteSanitation;
    }


    public static DeviceComplete deviceSanitation(DeviceComplete deviceComplete) {
        DeviceComplete deviceCompleteSanitation = null;
        deviceCompleteSanitation = new DeviceComplete(
                formatoLongSql(deviceComplete.getId_device()),
                formatoLongSql(deviceComplete.getDevice_number()),
                formatoLongSql(deviceComplete.getUuid()),
                formatoStringSql(deviceComplete.getDevice_type()),
                commonNotificationSanitation(deviceComplete.getCommon())
        );

        return deviceCompleteSanitation;
    }


    public static StateOrderComplete stateOrderCompleteSanitation(StateOrderComplete stateOrderComplete) {
        StateOrderComplete stateOrderCompleteSanitation = null;
        stateOrderCompleteSanitation = new StateOrderComplete(
                formatoLongSql(stateOrderComplete.getId_state()),
                formatoLIKESql(stateOrderComplete.getName_state()),
                formatoLIKESql(stateOrderComplete.getDescription()),
                formatoLIKESql(stateOrderComplete.getType_state()),
                commonStateSanitation(stateOrderComplete.getCommon())
        );

        return stateOrderCompleteSanitation;
    }

    public static SaleComplete saleCompleteSanitation(SaleComplete saleComplete) {
        SaleComplete saleCompleteSanitation = null;
        saleCompleteSanitation = new SaleComplete(
                formatoLongSql(saleComplete.getId_sale()),
                formatoLongSql(saleComplete.getId_employee()),
                formatoLongSql(saleComplete.getId_third()),
                formatoDateSql(saleComplete.getBegin_date()),
                formatoDateSql(saleComplete.getFinish_date()),
                formatoStringSql(saleComplete.getName_sale()),
                formatoStringSql(saleComplete.getDescription_sale()),
                formatoLongSql(saleComplete.getTotal_value()),
                formatoLongSql(saleComplete.getDiscount_percent()),
                stateOrderCompleteSanitation(saleComplete.getStateOrderComplete()),
                commonStateSanitation(saleComplete.getCommon())
        );

        return saleCompleteSanitation;
    }

    public static OrdersComplete ordersCompleteSanitation(OrdersComplete ordersComplete) {
        OrdersComplete ordersCompleteSanitation = null;
        ordersCompleteSanitation = new OrdersComplete(
                formatoLongSql(ordersComplete.getId_order()),
                formatoLongSql(ordersComplete.getId_third_client()),
                formatoLongSql(ordersComplete.getId_sale()),
                formatoLongSql(ordersComplete.getOrder_number()),
                stateOrderCompleteSanitation(ordersComplete.getStateOrderComplete()),
                commonStateSanitation(ordersComplete.getCommon())
        );

        return ordersCompleteSanitation;
    }

    public static DetailSaleComplete detailSaleCompleteSanitation(DetailSaleComplete detailSaleComplete) {
        DetailSaleComplete detailSaleCompleteSanitation = null;
        detailSaleCompleteSanitation = new DetailSaleComplete(
                formatoLongSql(detailSaleComplete.getId_detail_sale()),
                formatoLongSql(detailSaleComplete.getId_sale()),
                formatoLongSql(detailSaleComplete.getId_third()),
                formatoLongSql(detailSaleComplete.getId_product_third()),
                formatoLongSql(detailSaleComplete.getOriginal_value()),
                formatoLongSql(detailSaleComplete.getSale_value()),
                formatoIntegerSql(detailSaleComplete.getQuantity()),
                commonStateSanitation(detailSaleComplete.getCommon())
        );

        return detailSaleCompleteSanitation;
    }

    public static DetailOrderComplete detailOrderCompleteSanitation(DetailOrderComplete detailOrderComplete) {
        DetailOrderComplete detailOrderCompleteSanitation = null;
        detailOrderCompleteSanitation = new DetailOrderComplete(
                formatoLongSql(detailOrderCompleteSanitation.getId_detail_order()),
                formatoLongSql(detailOrderCompleteSanitation.getId_order()),
                formatoLongSql(detailOrderCompleteSanitation.getId_product_third()),
                formatoIntegerSql(detailOrderCompleteSanitation.getQuantity()),
                formatoLongSql(detailOrderCompleteSanitation.getId_third()),
                formatoLongSql(detailOrderCompleteSanitation.getValue_product()),
                commonStateSanitation(detailOrderCompleteSanitation.getCommon())
        );

        return detailOrderCompleteSanitation;
    }


}

