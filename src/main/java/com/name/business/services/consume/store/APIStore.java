package com.name.business.services.consume.store;


import com.name.business.services.consume.store.ProductThirdSimple;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

import java.util.List;

public interface APIStore {

    static final String BASE_URL = "http://localhost:8450/v1/";

    /**
     * Get info of third with @param idProductThird.
     * @param KEY_VALUE
     * @param id_product
     * @return
     */
    @GET("products-third/simple")
    Call<List<ProductThirdSimple>> getProductThird(@Header("Authorization") String KEY_VALUE, @Query("id_product") Long id_product);

}
