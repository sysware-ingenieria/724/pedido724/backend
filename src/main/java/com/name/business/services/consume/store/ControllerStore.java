package com.name.business.services.consume.store;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ControllerStore implements Callback<List<Object>> {

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIStore.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        APIStore apiService = retrofit.create(APIStore.class);

        Call<List<ProductThirdSimple>> call = apiService.getProductThird("status:open", new Long(1));
        //call.enqueue(this);

    }

    @Override
    public void onResponse(Call<List<Object>> call, Response<List<Object>> response) {
        if(response.isSuccessful()) {
            List<Object> read = response.body();
            read.forEach(each -> System.out.println(each));
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<List<Object>> call, Throwable t) {
        t.printStackTrace();
    }
}
