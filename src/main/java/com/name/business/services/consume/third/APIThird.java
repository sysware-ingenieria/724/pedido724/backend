package com.name.business.services.consume.third;


import com.name.business.services.consume.store.ProductThirdSimple;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

import java.util.List;

public interface APIThird {

    static final String BASE_URL = "http://localhost:8447/v1/";

    /**
     * Get info of third with @param idProductThird.
     * @param KEY_VALUE
     * @param id_third
     * @return
     */
    @GET("thirds")
    Call<List<Third>> getThird(@Header("Authorization") String KEY_VALUE, @Query("id_third") Long id_third);

}
