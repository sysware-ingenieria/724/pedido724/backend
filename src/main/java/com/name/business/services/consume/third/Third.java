package com.name.business.services.consume.third;

public class Third {

    private Long id_third;
    private Long id_third_father;
    private Object profile;
    private Object type;
    private Object state;
    private Object info;
    private Object directory;

    public Third(Long id_third, Long id_third_father, Object profile, Object type, Object state, Object info, Object directory) {
        this.id_third = id_third;
        this.id_third_father = id_third_father;
        this.profile = profile;
        this.type = type;
        this.state = state;
        this.info = info;
        this.directory = directory;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_third_father() {
        return id_third_father;
    }

    public void setId_third_father(Long id_third_father) {
        this.id_third_father = id_third_father;
    }

    public Object getProfile() {
        return profile;
    }

    public void setProfile(Object profile) {
        this.profile = profile;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getInfo() {
        return info;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public Object getDirectory() {
        return directory;
    }

    public void setDirectory(Object directory) {
        this.directory = directory;
    }
}
