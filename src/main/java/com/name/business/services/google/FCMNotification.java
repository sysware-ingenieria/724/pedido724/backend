package com.name.business.services.google;

import org.json.JSONObject;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class FCMNotification {

    // Method to send Notifications from server to client end.

    //Your FCM Authentication KEY
    private final static String AUTH_KEY_FCM = "AIzaSyDPvNJI0gcehcqKChjGDRDX-B5ZBPBkW2U";

    //API Service
    private final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public final static String PARAM_TO = "to";
    public final static String TITLE = "title";
    public final static String BODY = "body";
    public final static String SOUND = "sound";
    public final static String COLOR = "color";
    public final static String CLICK_ACTION = "clock_action";
    public final static String NOTIFICATION = "notification";
    public final static String DEFAULT = "default";
    public final static String DATA = "data";

    public String value_title;
    public String value_body;
    public String value_sound;
    public String value_clor;
    public String value_click_action;
    public String value_notification;
    public JSONObject info;
    public JSONObject apiInput;

    public FCMNotification() {
        this.value_title = "";
        this.value_body = "";
        this.value_sound = "";
        this.value_clor = "";
        this.value_click_action = "";
        this.value_notification = "";
        this.info = new JSONObject();
    }

    // userDeviceIdKey is the device id you will query from your database
    public void pushFCMNotification(String userDeviceIdKey) throws Exception {

        //URL Connection
        URL url = new URL(API_URL_FCM); //API URL
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        // HEADER
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM); //Authentication Key
        conn.setRequestProperty("Content-Type", "application/json");

        //BODY
        apiInput.put(PARAM_TO, userDeviceIdKey.trim());
        buildInfo("Notification Title", "Hello Test notification", DEFAULT, "#FFFF0000", "com.ingenieria.sysware.order724_TARGET_NOTIFICATION", null);
        buildJSON(info);

        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(apiInput.toString());
        wr.flush();
        conn.getInputStream();
    }

    public void buildJSON(JSONObject info) {
        apiInput.put(NOTIFICATION, info);
    }

    public void buildInfo(String value_title, String value_body, String value_sound, String value_color, String value_click_action, JSONObject valueData) {
        if (!value_title.equals(null))
            addParam(TITLE, value_title); // Notification title
        if (!value_body.equals(null))
            addParam(BODY, value_body); // Notification body
        if (!value_sound.equals(null))
            addParam(SOUND, value_sound);
        if (!value_color.equals(null))
            addParam(COLOR, value_color);
        if (!value_click_action.equals(null))
            addParam(CLICK_ACTION, value_click_action);
        if (valueData != null)
            addData(valueData);
        buildJSON(info);
    }

    public void addParam(String param, String value) {
        info.put(param, value);
    }

    public void addData(JSONObject value) {
        info.put(DATA, value);
    }

    public String getValue_title() {
        return value_title;
    }

    public void setValue_title(String value_title) {
        this.value_title = value_title;
    }

    public String getValue_body() {
        return value_body;
    }

    public void setValue_body(String value_body) {
        this.value_body = value_body;
    }

    public String getValue_sound() {
        return value_sound;
    }

    public void setValue_sound(String value_sound) {
        this.value_sound = value_sound;
    }

    public String getValue_clor() {
        return value_clor;
    }

    public void setValue_clor(String value_clor) {
        this.value_clor = value_clor;
    }

    public String getValue_click_action() {
        return value_click_action;
    }

    public void setValue_click_action(String value_click_action) {
        this.value_click_action = value_click_action;
    }

    public String getValue_notification() {
        return value_notification;
    }

    public void setValue_notification(String value_notification) {
        this.value_notification = value_notification;
    }

    public JSONObject getInfo() {
        return info;
    }

    public void setInfo(JSONObject info) {
        this.info = info;
    }

    /*
    public void main(String[] args) {
        try{
        pushFCMNotification("eAi8hlyf3nM:APA91bGY_ARBzzJnNgRsTmvvhGIC1SEswxz_oI-M4Xkre2yQsdEaRM96jx-xCmX3Sw_Qa7lD2FZWhJBoVwHo4yZ0N93shgH70VawZNR8KHMHrBI0SsN0erFM_8lQqRDNe_rxSXD4R2DI");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Fallo ");
        }

    }*/

}
