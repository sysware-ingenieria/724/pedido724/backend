package com.name.business.utils.Scheduler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Niki Ordoñez
 */
public class SchedulerTimer {

    // Hours * Minuts * Seconds * MilSeconds
    public static int ONCE_PER_DAY = 24*60*60*1000;

    // Hours * Minuts * Seconds * MilSeconds
    public static int ONCE_PER_WEEK = 7*24*60*60*1000;

    // Hours * Minuts * Seconds * MilSeconds
    public static int ONCE_PER_MONTH = 30*24*60*60*1000;

    // Hours * Minuts * Seconds * MilSeconds
    public static int ONCE_PER_MINUTE = 60*1000;

    // Hours * Minuts * Seconds * MilSeconds
    public static int ONCE_PER_HOUR = 60*60*1000;

    // Hours * Minuts * Seconds * MilSeconds
    public static int ONCE_PER_SECOND = 1000;

    public static void scheduleTask(TimerTask timerTask, String date, int timeFrequence) throws InterruptedException {
        Timer time = new Timer(); // Instantiate Timer Object
        time.schedule(timerTask, getDate(date), timeFrequence); // Create Repetitively task for every timeFrequence
    }

    public static Date getDate(int hour, int minute) {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, new Integer(hour));
        today.set(Calendar.MINUTE, new Integer(minute));
        return today.getTime();
    }

    public static Date getDate(String time){
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            date = new Date();
        }
        return date;
    }
}
