package com.name.business.utils.exeptions;


//Permite crear un mensaje personalizado al error
public interface IException {
    public String getMessage();
}
